"""gardenMicroservice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('garden/', include('garden.urls')),
	# # url(r'^$', views.get_name, name='get_name'),
    # # path('ajax/load-hardware/', views.load_hardware, name='ajax_load_hardware'),
    # url(r'^api/list/load_hardware', views.APILoadHardware.as_view()),
    # path('ajax/copied-hardware/', views.copied_hardware, name='ajax_copied_hardware'),
    # url(r'^api/list/garden/', views.APIGarden.as_view()),
    # url(r'^api/list/gardenplant/', views.APIGardenPlant.as_view()),
    # url(r'^api/list/plantmetrics/', views.APIPlantMetrics.as_view()),
    # url(r'^api/list/sensor/', views.APISensor.as_view()),
    # url(r'^api/list/control/', views.APIControl.as_view()),
    # url(r'^api/chart/data/$',views.ChartData.as_view()),
    # url(r'^api/table/gardendata/', views.GardenTable.as_view()),
    # url(r'^api/list/activetoken/', views.TokenLog.as_view()),

]



if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

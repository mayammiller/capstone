from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views


from community import views
app_name='community'

urlpatterns = [
    url(r'^$', views.QuestionListView.as_view(), name='community_home'),
    url(r'^tag/$', views.TagListView, name='tagged'),
    url(r'^questions/new/$', views.new_question, name='new_question'),
    #url(r'^questions/(?P<question_pk>\d+)/$', views.PostListView.as_view(), name='question_posts'),
    url(r'^questions/(?P<question_pk>\d+)/$', views.post_detail, name='question_posts'),
    # url(r'^questions/(?P<question_pk>\d+)/reply/$', views.reply_question, name='reply_question'),
    url(r'^questions/(?P<question_pk>\d+)/posts/(?P<post_pk>\d+)/edit/$', views.PostUpdateView.as_view(), name='edit_post'),
    url(r'^questions/tagged/(?P<tag_id>\d+)/$', views.tag_question, name='question_tags'),
    url(r'^posts/vote/$', views.upvote, name='upvote'),

]

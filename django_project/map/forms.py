from django import forms
from .models import Produce
from django.core import validators

def check_postal_code(value):
    #Add conditions
    #raise forms.ValidationError("INCORRECT POSTAL CODE")
    pass

class AddSaleItem(forms.ModelForm):
    botcatcher = forms.CharField(required=False,
                                widget=forms.HiddenInput,
                                validators=[validators.MaxLengthValidator(0)])
    class Meta:
        model = Produce
        fields =[
            'produceName',
            'unit',
            'producePrice',
            'address',
            'city',
            'postal'
        ]




    # produceName     = forms.CharField(label= 'Produce', widget=forms.TextInput(attrs={'placeholder': 'Spinach'}) )
    # unit            = forms.CharField(label= 'Sale Unit (bag, lbs, number, etc)', widget=forms.TextInput(attrs={'placeholder': 'bag'}) )
    # producePrice    = forms.DecimalField(label= 'Price',widget=forms.TextInput(attrs={'placeholder': '2.00'}) )
    # address         = forms.CharField(label= 'Address', widget=forms.TextInput(attrs={'placeholder': '123 Food St'}) )
    # city            = forms.CharField(label= 'City', widget=forms.TextInput(attrs={'placeholder': 'Calgary'}) )
    # postal          = forms.CharField(label= 'Postal Code',widget=forms.TextInput(attrs={'placeholder': 'T2G1G1'}) )

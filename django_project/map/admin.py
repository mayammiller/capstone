from django.contrib import admin
from map.models import Produce
#from leaflet import LeafletGeoAdmin
from leaflet.admin import LeafletGeoAdmin

# Register your models here.

class ProduceAdmin(LeafletGeoAdmin):
    list_display = ('produceName', 'loc')

admin.site.register(Produce, ProduceAdmin)
# admin.site.register(Produce)

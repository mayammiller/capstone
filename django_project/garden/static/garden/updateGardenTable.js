// var csrftoken = Cookies.get('csrftoken');

$(document).ready(function(){
    $.ajax({
        url: '/garden/api/table/gardendata/',
        type: 'get',
        success: function(data){
            console.log("successfully got data")
            console.log(data)
            data=JSON.parse(data)
            var event_data = '<tbody>';
            $.each(data, function(index, value){
                /*console.log(value);*/
                event_data += '<tr id=' + value.garden.id + '>';
                event_data += '<td>'+value.garden.gardenName+'</td>';
                event_data += '<td id=' + value.id +'>'+value.plantName+'</td>';
                event_data += '<td>'+value.plantQuantity+'</td>';
                event_data += '</tr>';
            });
            event_data += '</tbody>'
            console.log(event_data)
            $("#list_table_gardenData").append(event_data);
            selectedRowGarden()

        },
        error: function(d){
            console.log("error in Garden Data Table");
            // alert("404. Please wait until the File is Loaded.");
        }
    });
});

function gardenDeleteCheck(word) {
    var highlightedGarden;
    var highlightedPlant
    var table = document.getElementById('list_table_gardenData')
    for (var i=1; i<table.rows.length; i++){
      if(table.rows[i].contains("selected")){
        highlightedGarden = table.rows[i].id
        highlightedPlant = table.rows[i].getElementsByTagName("td")[1].id
      }
    }
    document.getElementById("gardenToDelete").innerHTML = highlightedGarden;
    // document.getElementById("plantToDelete").innerHTML = highlightedGarden;
  }

function selectedRowGarden(){
  var gardenID, plantID, index
  var table = document.getElementById('list_table_gardenData')
  for (var i=1; i<table.rows.length; i++){

    table.rows[i].onclick = function(){
      if (typeof index !=="undefined"){
        table.rows[index].classList.toggle("selected");
      }
      index=this.rowIndex;
      highlightedGarden = this.id;
      highlightedGardenName = this.getElementsByTagName("td")[0].innerHTML;
      // console.log(highlightedGarden)
      myGardenSpan = document.getElementsByClassName("gardenToDelete")[0]
      myGardenSpan.innerHTML = highlightedGardenName;
      myGardenSpan.id = highlightedGarden
      // console.log(highlightedGardenName)
      // console.log(highlightedGarden)
      this.classList.toggle("selected")
      highlightedPlant = this.getElementsByTagName("td")[1].id;
      highlightedPlantName = this.getElementsByTagName("td")[1].innerHTML;
      myPlantSpan = document.getElementsByClassName("plantToDelete")[0]
      myPlantSpan.innerHTML = highlightedPlantName;
      myPlantSpan.id = highlightedPlant

      console.log(highlightedPlant)
    }
  }
}

// $(document).ready(function(){
//   $.ajaxSetup({
//   beforeSend: function(xhr, settings) {
//       if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
//           xhr.setRequestHeader("X-CSRFToken", csrftoken);
//           console.log(csrftoken)
//       }
//   }
// });
// })

function deleteGarden(){
  myGardenSpan = document.getElementsByClassName("gardenToDelete")[0]
  gardenid = myGardenSpan.id
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});
  $.ajax({
      url: '/garden/api/table/gardendelete/',
      type: "DELETE",
      data: {
        // 'csrfmiddlewaretoken': getCookie(),
        'gardenid': gardenid       // add the country id to the GET parameters
      },
      success: function(data){
          console.log("successfully deleted")
      },
      error: function(d){
          console.log("error in delete");
          console.log(d)
          // alert("404. Please wait until the File is Loaded.");
      }
})}

function deletePlant(){
  myPlantSpan = document.getElementsByClassName("plantToDelete")[0]
  plantid = myPlantSpan.id
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});
  $.ajax({
      url: '/garden/api/table/plantdelete/',
      type: "DELETE",
      data: {
        // 'csrfmiddlewaretoken': getCookie(),
        'plantid': plantid       // add the country id to the GET parameters
      },
      success: function(data){
          console.log("successfully deleted")
      },
      error: function(d){
          console.log("error in delete");
          console.log(d)
          // alert("404. Please wait until the File is Loaded.");
      }
})}

// function getCookie(name) {
//     var cookieValue = null;
//     if (document.cookie && document.cookie !== '') {
//         var cookies = document.cookie.split(';');
//         for (var i = 0; i < cookies.length; i++) {
//             var cookie = cookies[i].trim();
//             // Does this cookie string begin with the name we want?
//             if (cookie.substring(0, name.length + 1) === (name + '=')) {
//                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
//                 break;
//             }
//         }
//     }
//     console.log(cookieValue)
//     return cookieValue;
// }
//
// function getCookie(){
//   // token = document.getElementsByTagName('meta').
//   token = document.querySelector("meta[name='_token']").getAttribute("content");
//   console.log(token)
//   return token
// }
// var csrftoken = getCookie('csrftoken');
//
// $(document).ready(function() {
//     var rows = document.getElementById("list_table_gardenData").rows
//     console.log("my rows")
//     console.log(rows)
//     // var rows = $('list_table_gardenData').$('tr');
//
//     // Create 'click' event handler for rows
//     // rows.addEventListener('click', function(e) {
//     // for (let row of rows){
//     //   console.log(row)
//     for (var i = 0; i < rows.length; i++) {
//     // rows.on('click',function(e){
//         // Get current row
//       // var row = $(this);
//       var row = rows[i]
//       row.click = function(e){
//       // row.click(function(e){
//          // Check if 'Ctrl', 'cmd' or 'Shift' keyboard key was pressed
//          // * 'Ctrl' => is represented by 'e.ctrlKey' or 'e.metaKey'
//          // * 'Shift' => is represented by 'e.shiftKey'
//         // if ((e.ctrlKey || e.metaKey) || e.shiftKey) {
//         //     // If pressed highlight the other row that was clicked
//         //     row.addClass('highlight');
//         // } else {
//         //     // Otherwise just highlight one row and clean others
//         this.style.color = "#ff0000";
//             rows.removeClass('highlight');
//             row.addClass('highlight');
//         // }
//     }}//);//);
//     // This 'event' is used just to avoid that the table text
//     //  * gets selected (just for styling).
//     //  * For example, when pressing 'Shift' keyboard key and clicking
//     //  * (without this 'event') the text of the 'table' will be selected.
//     //  * You can remove it if you want, I just tested this in
//     //  * Chrome v30.0.1599.69
//     $(document).bind('selectstart dragstart', function(e) {
//         e.preventDefault(); return false;
//     });
//
// });

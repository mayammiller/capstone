class Control(object):
    def __init__(self, garden, controlID, controlName, controlLocation, ispH, isHumidity, isLight, isWaterLevel, isNutrientConc, isTemperature):
        self.garden = garden
        self.controlID = controlID
        self.controlName = controlName
        self.controlLocation = controlLocation
        self.ispH = ispH
        self.isHumidity = isHumidity
        self.isLight = isLight
        self.isWaterLevel = isWaterLevel
        self.isNutrientConc = isNutrientConc
        self.isTemperature = isTemperature

class Sensor(object):
    def __init__(self, garden, sensorID, sensorName, sensorLocation, ispH, isHumidity, isLight, isWaterLevel, isNutrientConc, isTemperature):
        self.garden = garden
        self.sensorID = sensorID
        self.sensorName = sensorName
        self.sensorLocation = sensorLocation
        self.ispH = ispH
        self.isHumidity = isHumidity
        self.isLight = isLight
        self.isWaterLevel = isWaterLevel
        self.isNutrientConc = isNutrientConc
        self.isTemperature = isTemperature

class Garden(object):
    # username = models.IntegerField(request.user.id, on_delete=models.CASCADE)
    def __init__(self, userid = None, gardenName = None, id = None):
        self.userid = userid
        self.gardenName = gardenName
        self.id = id

class GardenPlant(object):

    def __init__(self, garden=None, plantName=None, plantQuantity=None, plantLocation=None, id=None):
        self.garden = garden
        self.plantName = plantName
        self.plantQuantity = plantQuantity
        self.plantLocation = plantLocation
        self.id = id

#
class PlantMetrics(object):

    def __init__(self, plant,humidityLow,humidityHigh,lightLow,lightHigh,tempLow,tempHigh,nutrientLow,nutrientHigh,waterLow,waterHigh,pHLow,pHHigh):
        self.plant = plant
        self.humidityLow = humidityLow
        self.humidityHigh = humidityHigh
        self.lightLow = lightLow
        self.lightHigh = lightHigh
        self.tempLow = tempLow
        self.tempHigh = tempHigh
        self.nutrientLow = nutrientLow
        self.nutrientHigh = nutrientHigh
        self.waterLow = waterLow
        self.waterHigh = waterHigh
        self.pHLow = pHLow
        self.pHHigh =pHHigh
    #
#     def __str__(self):
#         return self.plantName.plantName
#
# class Sensor(models.Model):
#     gardenName = models.ForeignKey(Person, on_delete=models.CASCADE)
#     sensorID = models.CharField(max_length=50, blank=True)
#     sensorName = models.CharField(max_length=50)
#     sensorLocation = models.CharField(max_length=50, blank=True)
#     ispH = models.BooleanField(default = 'false')
#     isHumidity = models.BooleanField(default = 'false')
#     isLight = models.BooleanField(default = 'false')
#     isWaterLevel = models.BooleanField(default = 'false')
#     isNutrientConc = models.BooleanField(default = 'false')
#     isTemperature = models.BooleanField(default = 'false')
#
#     def __str__(self):
#         return f'{self.gardenName.username.username}: {self.sensorName}: {self.sensorID}'
#
# class Control(models.Model):
#     gardenName = models.ForeignKey(Person, on_delete=models.CASCADE)
#     controlID = models.CharField(max_length=50)
#     controlName = models.CharField(max_length=50)
#     controlLocation = models.CharField(max_length=50, blank=True)
#     ispH = models.BooleanField(default = 'false')
#     isHumidity = models.BooleanField(default = 'false')
#     isLight = models.BooleanField(default = 'false')
#     isWaterLevel = models.BooleanField(default = 'false')
#     isNutrientConc = models.BooleanField(default = 'false')
#     isTemperature = models.BooleanField(default = 'false')
#
#     def __str__(self):
#         return f'{self.gardenName.username.username}: {self.controlName}: {self.controlID}'
#
# class ControlStatus(models.Model):
#     controlID = models.ForeignKey(Control, on_delete=models.CASCADE)
#     status = models.CharField(max_length=50)
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     class Meta:
#         verbose_name_plural = "ControlStatuses"
#
#     def __str__(self):
#         return f'{self.controlName}: {self.controlID}'
#
#
# class LightSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class PHSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class TempSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class HumiditySensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class WaterLevelSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class NutrientSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'

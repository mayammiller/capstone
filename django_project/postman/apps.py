from django.apps import AppConfig


class MessagingConfig(AppConfig):
    name = 'postman'

    def ready(self):
        from .models import setup
        setup()
# from django.db import models
# from django.contrib.auth.models import User
#
# # Create your models here.
#
# class Person(models.Model):
#     # username = models.IntegerField(request.user.id, on_delete=models.CASCADE)
#     username = models.ForeignKey(User, on_delete=models.CASCADE)
#     gardenName = models.CharField(max_length=150)
#
#     def __str__(self):
#         return f'{self.id}:{self.username.username}: {self.gardenName}'
#
# class Garden(models.Model):
#     gardenName = models.ForeignKey(Person, on_delete=models.CASCADE)
#     plantName = models.CharField(max_length=50)
#     plantQuantity = models.IntegerField(default=0)
#     plantLocation = models.CharField(max_length=50, blank=True)
#
#     def __str__(self):
#         return f' {self.gardenName.gardenName}: {self.plantName}'
#
# class Plant(models.Model):
#     #username = models.ForeignKey(Person, on_delete=models.CASCADE)
#     plantName = models.ForeignKey(Garden, on_delete=models.CASCADE)
#     humidityLow = models.FloatField(blank=True)
#     humidityHigh = models.FloatField(blank=True)
#     lightLow = models.FloatField(blank=True)
#     lightHigh = models.FloatField(blank=True)
#     tempLow = models.FloatField(blank=True)
#     tempHigh = models.FloatField(blank=True)
#     nutrientLow = models.FloatField(blank=True)
#     nutrientHigh = models.FloatField(blank=True)
#     waterLow = models.FloatField(blank=True)
#     waterHigh = models.FloatField(blank=True)
#     pHLow = models.FloatField(blank=True)
#     pHHigh = models.FloatField(blank=True)
#
#     def __str__(self):
#         return self.plantName.plantName
#
# class Sensor(models.Model):
#     gardenName = models.ForeignKey(Person, on_delete=models.CASCADE)
#     sensorID = models.CharField(max_length=50, blank=True)
#     sensorName = models.CharField(max_length=50)
#     sensorLocation = models.CharField(max_length=50, blank=True)
#     ispH = models.BooleanField(default = 'false')
#     isHumidity = models.BooleanField(default = 'false')
#     isLight = models.BooleanField(default = 'false')
#     isWaterLevel = models.BooleanField(default = 'false')
#     isNutrientConc = models.BooleanField(default = 'false')
#     isTemperature = models.BooleanField(default = 'false')
#
#     def __str__(self):
#         return f'{self.gardenName.username.username}: {self.sensorName}: {self.sensorID}'
#
# class Control(models.Model):
#     gardenName = models.ForeignKey(Person, on_delete=models.CASCADE)
#     controlID = models.CharField(max_length=50)
#     controlName = models.CharField(max_length=50)
#     controlLocation = models.CharField(max_length=50, blank=True)
#     ispH = models.BooleanField(default = 'false')
#     isHumidity = models.BooleanField(default = 'false')
#     isLight = models.BooleanField(default = 'false')
#     isWaterLevel = models.BooleanField(default = 'false')
#     isNutrientConc = models.BooleanField(default = 'false')
#     isTemperature = models.BooleanField(default = 'false')
#
#     def __str__(self):
#         return f'{self.gardenName.username.username}: {self.controlName}: {self.controlID}'
#
# class ControlStatus(models.Model):
#     controlID = models.ForeignKey(Control, on_delete=models.CASCADE)
#     status = models.CharField(max_length=50)
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     class Meta:
#         verbose_name_plural = "ControlStatuses"
#
#     def __str__(self):
#         return f'{self.controlName}: {self.controlID}'
#
#
# class LightSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class PHSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class TempSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class HumiditySensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class WaterLevelSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'
#
# class NutrientSensor(models.Model):
#     sensorID = models.ForeignKey(Sensor, on_delete=models.CASCADE)
#     value = models.FloatField()
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return f'{self.sensorID}'

var myElementsToView = {}
$("#id_gardenNameView").change(function () {  //purpose of this script is to load the available sensors and controls for a select garden, specifically for form3
  var url = $("#hardwareForm").attr("data-hardware-url");  // get the url of the `load_cities` view
  var garden_id = $(this).val();  // get the selected country ID from the HTML input
  console.log(url)
  console.log("Garden ID: " + garden_id)
  $.ajax({                      // initialize an AJAX request
    url: url,                    // set the url of the request (= localhost:8000/hr/ajax/load-cities/)
    data: {
      'gardenName': garden_id       // add the country id to the GET parameters
    },
    success: function (data) {   // `data` is the return of the `load_cities` view function
      $("#id_sensorListView").html(data);  // replace the contents of the city input with the data that came from the server
      console.log(data)
    }
  });

});

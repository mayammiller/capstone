$(document).ready(function(){
    $.ajax({
        url: '/garden/api/table/sensordata/',
        type: 'get',
        success: function(data){
            console.log("successfully got sensor data")
            $("#list_table_sensorData").append(data);
            selectedRow()

        },
        error: function(d){
            console.log("error in Sensor Data Table");
            // alert("404. Please wait until the File is Loaded.");
        }
    });
});

// function sensorDeleteCheck(word) {
//     var highlightedSensor;
//     var table = document.getElementById('list_table_sensorData')
//     for (var i=1; i<table.rows.length; i++){
//       if(table.rows[i].contains("selected")){
//         highlightedSensor = table.rows[i].getElementsByTagName("td")[1].id
//       }
//     }
//     document.getElementById("sensorToDelete").innerHTML = highlightedSensor;
//     // document.getElementById("plantToDelete").innerHTML = highlightedGarden;
//   }

function selectedRow(){
  var gardenID, plantID, index
  var table = document.getElementById('list_table_sensorData')
  for (var i=1; i<table.rows.length; i++){

    table.rows[i].onclick = function(){
      if (typeof index !=="undefined"){
        table.rows[index].classList.toggle("selected");
      }
      index=this.rowIndex;
      highlightedSensor = this.getElementsByTagName("td")[1].id
      highlightedSensorName = this.getElementsByTagName("td")[1].innerHTML;
      myGardenSpan = document.getElementsByClassName("sensorToDelete")[0]
      myGardenSpan.innerHTML = highlightedSensorName;
      myGardenSpan.id = highlightedSensor
      this.classList.toggle("selected")

      console.log(highlightedSensor)
    }
  }
}

function deleteSensor(){
  mySensorSpan = document.getElementsByClassName("sensorToDelete")[0]
  sensorid = mySensorSpan.id
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});
  $.ajax({
      url: '/garden/api/table/sensordelete/',
      type: "DELETE",
      data: {
        // 'csrfmiddlewaretoken': getCookie(),
        'sensorid': sensorid       // add the country id to the GET parameters
      },
      success: function(data){
          console.log("successfully deleted")
      },
      error: function(d){
          console.log("error in delete");
          console.log(d)
          // alert("404. Please wait until the File is Loaded.");
      }
})}

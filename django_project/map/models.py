from django.db import models
from django.contrib.gis.db import models
from geopy import geocoders
from geopy.exc import GeocoderTimedOut
from django.contrib.gis.geos import Point



# Create your models here.
class Produce(models.Model):
    produceName     = models.CharField(max_length=20, default='')
    unit            = models.CharField(max_length=50, default='')
    producePrice    = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True, default=0.00)
    address         = models.CharField(max_length=20, default='')
    city            = models.CharField(max_length=20, default='')
    postal          = models.CharField(max_length=20, default='')
    loc             = models.PointField(default=Point(-114.080251,51.007831)) #WHAT IS THIS SRID????



    #objs    = models.GeoManager()

    # @property
    # def popupContent(self):
    #   return '<p><{}</p>'.format(self.produceName)

    def  __str__(self):
        return self.produceName


    #NOT WORKING YET :)
    # def geocode_address(self, address):
    # """Google Maps v3 API: https://developers.google.com/maps/documentation/geocoding/"""
    # # https://stackoverflow.com/questions/27914648/geopy-catch-timeout-error
    # googlelocator  = GoogleV3()
    # try:
    #     loc = geolocator.geocode(address, exactly_one=True, timeout=5)
    # except GeocoderTimedOut as e:
    #     print("GeocoderTimedOut: geocode failed on input %s with message %s" % (address, e.msg))
    # except AttributeError as e:
    #     print("AttributeError: geocode failed on input %s with message %s" % (address, e.msg))

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class ActiveUser(models.Model):
    token = models.CharField(max_length=150, primary_key=True)
    userid = models.IntegerField()


class Garden(models.Model):
    userid = models.IntegerField()
    gardenName = models.CharField(max_length=150)

    class Meta:
        unique_together = (("userid", "gardenName"),)

    def __str__(self):
        return f'{self.id}:{self.gardenName}'

class GardenPlant(models.Model):
    garden = models.ForeignKey(Garden, on_delete=models.CASCADE)
    plantName = models.CharField(max_length=50)
    plantQuantity = models.IntegerField(default=0)
    plantLocation = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return f' {self.garden.gardenName}: {self.plantName}'

class PlantMetrics(models.Model):
    #username = models.ForeignKey(Person, on_delete=models.CASCADE)
    plant = models.ForeignKey(GardenPlant, on_delete=models.CASCADE)
    humidityLow = models.FloatField(blank=True)
    humidityHigh = models.FloatField(blank=True)
    lightLow = models.FloatField(blank=True)
    lightHigh = models.FloatField(blank=True)
    tempLow = models.FloatField(blank=True)
    tempHigh = models.FloatField(blank=True)
    nutrientLow = models.FloatField(blank=True)
    nutrientHigh = models.FloatField(blank=True)
    waterLow = models.FloatField(blank=True)
    waterHigh = models.FloatField(blank=True)
    pHLow = models.FloatField(blank=True)
    pHHigh = models.FloatField(blank=True)

    def __str__(self):
        return self.plant.plantName

class Sensor(models.Model):
    garden = models.ForeignKey(Garden, on_delete=models.CASCADE)
    sensorID = models.CharField(max_length=50, blank=True)
    sensorName = models.CharField(max_length=50)
    sensorLocation = models.CharField(max_length=50, blank=True)
    ispH = models.BooleanField(default = 'false')
    isHumidity = models.BooleanField(default = 'false')
    isLight = models.BooleanField(default = 'false')
    isWaterLevel = models.BooleanField(default = 'false')
    isNutrientConc = models.BooleanField(default = 'false')
    isTemperature = models.BooleanField(default = 'false')

    class Meta:
        unique_together = (("garden","sensorID"),)

    def __str__(self):
        return f'{self.garden.userid}: {self.sensorName}: {self.sensorID}'

class Control(models.Model):
    garden = models.ForeignKey(Garden, on_delete=models.CASCADE)
    controlID = models.CharField(max_length=50)
    controlName = models.CharField(max_length=50)
    controlLocation = models.CharField(max_length=50, blank=True)
    ispH = models.BooleanField(default = 'false')
    isHumidity = models.BooleanField(default = 'false')
    isLight = models.BooleanField(default = 'false')
    isWaterLevel = models.BooleanField(default = 'false')
    isNutrientConc = models.BooleanField(default = 'false')
    isTemperature = models.BooleanField(default = 'false')

    class Meta:
        unique_together = (("garden","controlID"),)

    def __str__(self):
        return f'{self.garden.userid}: {self.controlName}: {self.controlID}'

class ControlStatus(models.Model):
    control = models.ForeignKey(Control, on_delete=models.CASCADE)
    status = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "ControlStatuses"
        unique_together = (("control","timestamp"),)

    def __str__(self):
        return f'{self.control.controlName}: {self.control.controlID}'


class LightSensor(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    value = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (("sensor","timestamp"),)

    def __str__(self):
        return f'{self.sensor.sensorName}: {self.sensor.sensorID}'

class PHSensor(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    value = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (("sensor","timestamp"),)

    def __str__(self):
        return f'{self.sensor.sensorName}: {self.sensor.sensorID}'

class TempSensor(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    value = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (("sensor","timestamp"),)

    def __str__(self):
        return f'{self.sensor.sensorName}: {self.sensor.sensorID}'

class HumiditySensor(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    value = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (("sensor","timestamp"),)

    def __str__(self):
        return f'{self.sensor.sensorName}: {self.sensor.sensorID}'

class WaterLevelSensor(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    value = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (("sensor","timestamp"),)

    def __str__(self):
        return f'{self.sensor.sensorName}: {self.sensor.sensorID}'

class NutrientSensor(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    value = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (("sensor","timestamp"),)

    def __str__(self):
        return f'{self.sensor.sensorName}: {self.sensor.sensorID}'

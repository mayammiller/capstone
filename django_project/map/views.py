from django.shortcuts import render
from django.views.generic import TemplateView
from django.core.serializers import serialize
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Produce
from .forms import AddSaleItem

def MapView(request):
    if request.method == "POST":
        form = AddSaleItem(request.POST or None)
        if form.is_valid():
            print("Your item has been added to the database!")
            form.save(commit=True)
            #obj = form.save(commit=False)
            #obj.save()
        return redirect('map:map_home')
    else:
        form=AddSaleItem()

    return render(request, 'map/index.html', {'form': form})

def produce_locations(request):
    produce_locs = serialize('geojson', Produce.objects.all())
    return HttpResponse(produce_locs, content_type='json')

import math

from django.contrib.auth.models import User
from django.db import models
from django.utils.html import mark_safe
from django.utils.text import Truncator
from django.urls import reverse

from markdown import markdown
from taggit.managers import TaggableManager
import markdown_deux



class Question(models.Model):
    subject = models.CharField(max_length=255)
    last_updated = models.DateTimeField(auto_now_add=True)
    # topic = models.ForeignKey(Topic, related_name='questions', on_delete=models.CASCADE,)
    message = models.CharField(max_length=4000)
    starter = models.ForeignKey(User, related_name='questions', on_delete=models.CASCADE,)
    views = models.PositiveIntegerField(default=0)
    tags = TaggableManager()


    def __str__(self):
        return self.subject

    def get_page_count(self):
        count = self.posts.count()
        pages = count / 20
        return math.ceil(pages)

    def has_many_pages(self, count=None):
        if count is None:
            count = self.get_page_count()
        return count > 6

    def get_page_range(self):
        count = self.get_page_count()
        if self.has_many_pages(count):
            return range(1, 5)
        return range(1, count + 1)

    def get_last_ten_posts(self):
        return self.posts.order_by('-created_at')[:10]


class Post(models.Model):
    message = models.TextField(max_length=4000)
    question = models.ForeignKey(Question, related_name='posts', on_delete=models.CASCADE,)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True)
    created_by = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE,)
    updated_by = models.ForeignKey(User, null=True, related_name='+', on_delete=models.CASCADE,)
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.CASCADE,)
    votes_total = models.IntegerField(default=0)
    upvote = models.ManyToManyField(User, blank=True, related_name='upvote')
    downvote = models.ManyToManyField(User, blank=True, related_name='downvote')


    class Meta:
        ordering=['-created_at']

    def __str__(self):
        truncated_message = Truncator(self.message)
        return truncated_message.chars(30)

    def get_message_as_markdown(self):
        return mark_safe(markdown(self.message, safe_mode='escape'))

    def get_absolute_url(self):
        return reverse("community:question_posts", kwargs={"question_pk": self.question.pk})

    def children(self): #replies
        return Post.objects.filter(parent=self)

    @property
    def is_parent(self):
        if self.parent is not None:
            return False
        return True

    def total_votes(self):
        self.votes_total = self.upvote - self.downvote
        return self.votes_total






class Vote(models.Model):
    postID = models.ForeignKey(Post,on_delete=models.CASCADE)
    userID = models.ForeignKey(User,on_delete=models.CASCADE)

    class Meta:
        unique_together = ('postID', 'userID')

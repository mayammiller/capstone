from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile, City
from localflavor.ca.forms import CAPostalCodeField
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


class UserRegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required Field.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required Field.')
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("This email already exits!")
        return email

    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']
        if first_name.isalpha():
            return first_name
        else:
            raise ValidationError(_('Only alphabet characters are allowed'))

    def clean_last_name(self):
        last_name = self.cleaned_data['last_name']
        if last_name.isalpha():
            return last_name
        else:
            raise ValidationError(_('Only alphabet characters are allowed'))


class ProfileRegisterForm(forms.ModelForm):
    postal_code = CAPostalCodeField(required=False)

    class Meta:
        model = Profile
        fields = ['street_address', 'province', 'city', 'postal_code', 'image']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
       # self.fields['city'].queryset = City.objects.none()

        # if self.instance.province is not None:
        #     try:
        #         province_id = int(self.data.get('province'))
        #         self.fields['city'].queryset = City.objects.filter(province_id=province_id).order_by('name')
        #     except (ValueError, TypeError):
        #         pass
        if 'province' in self.data:
            try:
                province_id = int(self.data.get('province'))
                self.fields['city'].queryset = City.objects.filter(province_id=province_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['city'].queryset = self.instance.province.city_set.order_by('name')


class UserUpdateForm(forms.ModelForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required Field.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required Field.')
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['email'].widget.attrs['readonly'] = True

    def clean_email(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.email
        else:
            return self.cleaned_data['email']

    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']
        if first_name.isalpha():
            return first_name
        else:
            raise ValidationError(_('Only alphabet characters are allowed'))

    def clean_last_name(self):
        last_name = self.cleaned_data['last_name']
        if last_name.isalpha():
            return last_name
        else:
            raise ValidationError(_('Only alphabet characters are allowed'))


class ProfileUpdateForm(forms.ModelForm):
    postal_code = CAPostalCodeField(required=False)

    class Meta:
        model = Profile
        fields = ['street_address', 'province', 'city', 'postal_code', 'image']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance.province is not None:
            try:
                province_id = int(self.data.get('province'))
                self.fields['city'].queryset = City.objects.filter(province_id=province_id).order_by('name')
            except (ValueError, TypeError):
                pass
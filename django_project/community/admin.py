from django.contrib import admin
from .models import Question, Post

# admin.site.register(Topic)
admin.site.register(Question)
admin.site.register(Post)

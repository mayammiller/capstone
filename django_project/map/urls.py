from django.urls import path, include
from django.views.generic import TemplateView
from . import views

app_name = 'map'

urlpatterns =[
    # path(r'', TemplateView.as_view(template_name='map/index.html'), name='map_home'),
    path(r'', views.MapView, name='map_home'),
    path('produce_data/', views.produce_locations, name='loc'),
]

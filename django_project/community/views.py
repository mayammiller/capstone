from django.db.models import Count
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import UpdateView, ListView, RedirectView
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.urls import reverse
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

from .forms import NewQuestionForm, PostForm
from .models import Post, Question
from taggit.models import TaggedItemBase, Tag, TaggedItem
import json


###### METHOD TO GET ALL TAGS #############
class TagMixin(object):
    def get_context_data(self, **kwargs):
        context = super(TagMixin, self).get_context_data(**kwargs)
        context['tags'] = Tag.objects.filter()
        return context


###########RETRIEVES ALL TAGS ASSOCIATED WITH A SPECFIC QUESTION ##############
class TagIndexView(TagMixin, ListView):
    template_name = 'community/tags.html'
    model = Question
    paginate_by = '10'
    context_object_name = 'questions'

    def get_queryset(self):
        return Question.objects.filter(tags__slug=self.kwargs.get('slug'))


######## DISPLAYS ALL TAGS EXISTING WITH THEIR COUNT #############
def TagListView(request):
    all_tags = Tag.objects.all()
    tag_count = all_tags.annotate(num_times=Count('taggit_taggeditem_items'))

    context = {
        "tags": tag_count,
    }

    return render(request, "community/tags.html", context)



########### QUESTIONS ASSOCIATED WITH A TAG ##############
def tag_question(request, tag_id):

    queryset = Question.objects.filter(tags__id=tag_id)
    tag_name = Tag.objects.filter(id=tag_id)

    context = {
        "object_list": queryset,
        "tag_name": tag_name
    }
    print(context)

    return render(request, "community/question_tags.html", context)

################ SEARCH ALL TAGS - NOT WORKING YET :D #################
# def search(request):
#     found_entries = None
#     if ('q' in request.GET) and request.GET['q'].strip():
#         query_string = request.GET['q']
#         entry_query = get_query(query_string, ['company_name', 'stock_symbol', 'address', 'tags'])
#         found_entries = Company.objects.filter(entry_query).order_by('company_name')\
#             .annotate(num_transcripts=Count('transcripts'))
#
#     return render_to_response('company_list.html', {'all_companies': found_entries})

########## DISPLAYS ALL QUESTIONS ############
class QuestionListView(ListView):
    model = Question
    context_object_name = 'questions'
    template_name = 'community/questions.html'
    paginate_by = 100

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.filter(Q(subject__icontains=query) | Q(tags__name__in=query) | Q(message__icontains=query)).distinct()
        else:
            object_list = self.model.objects.all()
        return object_list


#
######### METHOD THAT CREATES A NEW POST FORM ##############
@login_required
def new_question(request):
    if request.method == 'POST':
        form = NewQuestionForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.starter = request.user
            question.save()
            form.save_m2m()
            Post.objects.create(
                message=form.cleaned_data.get('message'),
                question=question,
                created_by=request.user
            )
            return redirect('community:community_home')
    else:
        form = NewQuestionForm()
    return render(request, 'community/new_question.html', {'form': form})


######## METHOD THAT SHOWS POSTS/REPLIES THAT ARE ASSOCIATED WITH A QUESTION ########
class PostListView(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'community/question_posts.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        session_key = 'viewed_topic_{}'.format(self.question.pk)
        if not self.request.session.get(session_key, False):
            self.question.views += 1
            self.question.save()
            self.request.session[session_key] = True
        kwargs['question'] = self.question
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        self.question = get_object_or_404(Question, pk=self.kwargs.get('question_pk'))
        queryset = self.question.posts.order_by('created_at')
        return queryset



def post_detail(request, question_pk):
    question = get_object_or_404(Question, pk=question_pk)
    session_key = 'viewed_topic_{}'.format(question_pk)
    if not request.session.get(session_key, False):
        question.views += 1
        question.save()
        request.session[session_key] = True


    form = PostForm(request.POST or None)
    if form.is_valid():
        parent_obj = None
        try:
            parent_id = int(request.POST.get("parent_id"))
            print(parent_id)
        except:
            parent_id = None

        if parent_id:
            parent_qs = Post.objects.filter(pk=parent_id)
            if parent_qs.exists() and parent_qs.count() == 1:
                parent_obj=parent_qs.first()

        post_obj = form.save(commit=False)
        post_obj.created_by=request.user
        post_obj.question = question
        post_obj.parent = parent_obj
        post_obj.save()



        return HttpResponseRedirect(post_obj.get_absolute_url())

    post = Post.objects.filter(question=question_pk).filter(parent=None)

    context = {
        "instance": question,
        "posts": post,
        "posts_form": form,

    }

    return render(request, "community/question_posts.html", context)





def upvote(request):
    if request.user.is_authenticated:
        post_pk = request.POST.get("id")
        vote_type = request.POST.get("vote_type")
        liked_post = Post.objects.get(pk=post_pk)
        user = request.user

        if vote_type == "up":
            if liked_post.upvote.filter(id=user.id).exists():
                # user has already liked this company
                # remove like/user
                liked_post.upvote.remove(user)
                liked_post.votes_total -= 1
                message = 'You disliked this'
                liked_post.save()

            else:
                # add a new like for a company
                liked_post.upvote.add(user)
                liked_post.votes_total += 1
                message = 'You liked this'
                liked_post.save()
        else:
            if liked_post.downvote.filter(id=user.id).exists():
                # user has already liked this company
                # remove like/user
                liked_post.downvote.remove(user)
                liked_post.votes_total += 1
                message = 'You disliked this'
                liked_post.save()

            else:
                # add a new like for a company
                liked_post.downvote.add(user)
                liked_post.votes_total -= 1
                message = 'You liked this'
                liked_post.save()


        ctx = {'vote_count': liked_post.votes_total}
        return HttpResponse(json.dumps(liked_post.votes_total)) # Sending an success response
    # else:
    #     return JsonResponse("Request method is not a GET")


        # if request.method == 'GET':
        #        post_id = request.GET.get('post_pk')
        #
        #        likedpost = Post.objects.get(pk=post_id) #getting the liked posts
        #        user = request.user
        #
        #        if likedpost.upvote.filter(id=user.id).exists():
        #             # user has already liked this company
        #             # remove like/user
        #             likedpost.votes_total -= 1
        #             message = 'You disliked this'
        #             likedpost.save()
        #
        #        else:
        #             # add a new like for a company
        #             likedpost.upvote.add(user)
        #             likedpost.votes_total += 1
        #             message = 'You liked this'
        #             likedpost.save()
        #
        #        ctx = [{'vote_count': likedpost.votes_total, 'message': message}]
        #        return JsonResponse(ctx, safe=False) # Sending an success response
        # else:
        #        return JsonResponse("Request method is not a GET")


#######METHOD TO REPLY TO A POST ########## as of now you can reply to your own post?
# @login_required
# def reply_question(request, question_pk):
#     question = get_object_or_404(Question, pk=question_pk)
#     if request.method == 'POST':
#         form = PostForm(request.POST)
#         if form.is_valid():
#             post = form.save(commit=False)
#             post.question = question
#             post.created_by = request.user
#             post.save()
#
#             question.last_updated = timezone.now()
#             question.save()
#
#             question_url = reverse('community:question_posts', kwargs={'question_pk': question_pk})
#             question_post_url = '{url}?page={page}#{id}'.format(
#                 url=question_url,
#                 id=post.pk,
#                 page=question.get_page_count()
#             )
#
#             return redirect(question_post_url)
#     else:
#         form = PostForm()
#     return render(request, 'community/reply_question.html', {'question': question, 'form': form})
#

########## METHOD TO EDIT YOUR OWN POST ######## must be user that created it
@method_decorator(login_required, name='dispatch')
class PostUpdateView(UpdateView):
    model = Post
    fields = ('message', )
    template_name = 'community/edit_post.html'
    pk_url_kwarg = 'post_pk'
    context_object_name = 'post'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(created_by=self.request.user)

    def form_valid(self, form):
        post = form.save(commit=False)
        post.updated_by = self.request.user
        post.updated_at = timezone.now()
        post.save()
        return redirect('community:question_posts', question_pk=post.question.pk)

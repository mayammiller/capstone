"""loginMicroservice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_auth.views import PasswordResetView, PasswordResetConfirmView
from rest_framework.routers import DefaultRouter, SimpleRouter

from users.views import FacebookLogin, UserViewSet

router = DefaultRouter()
router1 = SimpleRouter()
router1.register(r'profile', UserViewSet)

urlpatterns = [
    # Admin
    path('admin/', admin.site.urls),
    # Oauth2 Authentication
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    # Login Services API
    path('authentication/', include('users.urls')),
    # CRUD - C
    path(r'', include(router1.urls)),

    # Pwd reset and Social Login
    path('rest-auth/', include('rest_auth.urls')),  # pwd reset
    url(r'^account/', include('allauth.urls')),
    path('reset-password/', PasswordResetView.as_view(), name='reset-password'),
    path('reset-password-confirm/', PasswordResetConfirmView.as_view(), name='reset-password-confirm'),

    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login')
]
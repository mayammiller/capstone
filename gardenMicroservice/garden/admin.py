from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Garden)
admin.site.register(GardenPlant)
admin.site.register(PlantMetrics)
admin.site.register(Sensor)
admin.site.register(Control)
admin.site.register(ControlStatus)
admin.site.register(LightSensor)
admin.site.register(PHSensor)
admin.site.register(TempSensor)
admin.site.register(HumiditySensor)
admin.site.register(WaterLevelSensor)
admin.site.register(NutrientSensor)

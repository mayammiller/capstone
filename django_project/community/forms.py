from django import forms

from .models import Post, Question


class NewQuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = ['subject', 'message', 'tags']


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['message', ]

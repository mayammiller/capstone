import json
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
import requests
from .serializers import UserSerializer
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView

CLIENT_ID = 'JHV6lItFfXELfPlxQJkK5zG7CWAYRuJfK6h5NnYr'
CLIENT_SECRET = 'haIQOGS9gaBnVzWtWf1tolfoIHvYXREKk5NcOTf54uyqj3ycShtd3qZCZNIi4TgkoX0tVoKagB6Zucmrs4unaYgvufGkaMUtRiHkGZ8ysikW1WH8VCUi2wmHJS56RNgH'


# API endpoint for user registration
@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
    serializer = UserSerializer(data=request.data)
    req = json.loads(json.dumps(request.data))
    if serializer.is_valid():
        serializer.save()
        # Once user is registered we get a token for the created user.
        r = requests.post('http://127.0.0.1:8000/o/token/',
                          data={
                              'grant_type': 'password',
                              'username': req['username'],
                              'password': req['password'],
                              'client_id': CLIENT_ID,
                              'client_secret': CLIENT_SECRET,
                          },
                          )

        # Once we get tokenInfo then fetch user details to be passed as user info
        if r.status_code == 200:
            result = r.json()
            r2 = requests.get("http://127.0.0.1:8000/authentication/me",
                              headers={"Authorization": "Bearer " + result['access_token']})
            r1 = dict()
            r1['tokenInfo'] = result
            r1['userInfo'] = r2.json()
            return Response(r1)
    return Response(serializer.errors)


# API endpoint for user login
@api_view(['POST'])
@permission_classes([AllowAny])
def token(request):
    r = requests.post('http://127.0.0.1:8000/o/token/',
                      data={
                          'grant_type': 'password',
                          'username': request.data['username'],
                          'password': request.data['password'],
                          'client_id': CLIENT_ID,
                          'client_secret': CLIENT_SECRET,
                      },
                      )
    if r.status_code == 200:  # once logged in bind tokenInfo with userInfo
        result = r.json()
        r2 = requests.get("http://127.0.0.1:8000/authentication/me", headers={"Authorization": "Bearer "+result['access_token']})
        r1 = dict()
        r1['tokenInfo'] = result
        r1['userInfo'] = r2.json()
        return Response(r1)
    return Response(r.status_code)


# API endpoint for token refresh
@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request):
    '''
    Registers user to the server. Input should be in the format:
    {"refresh_token": "<token>"}
    '''
    r = requests.post('http://127.0.0.1:8000/o/token/',
                      data={
                          'grant_type': 'refresh_token',
                          'refresh_token': request.data['refresh_token'],
                          'client_id': CLIENT_ID,
                          'client_secret': CLIENT_SECRET,
                      },
                      )
    return Response(r.json())


# API endpoint for logging out user
@api_view(['POST'])
@permission_classes([AllowAny])
def revoke_token(request):
    '''
    Method to revoke tokens.
    {"token": "<token>"}
    '''
    r = requests.post('http://127.0.0.1:8000/o/revoke_token/',
                      data={
                          'token': request.data['token'],
                          'client_id': CLIENT_ID,
                          'client_secret': CLIENT_SECRET,
                      },
                      )
    if r.status_code == requests.codes.ok:
        return Response({'message': 'token revoked'}, r.status_code)
    return Response(r.json(), r.status_code)


# API endpoint for CRUD - view, delete and update.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


# TODO API for login via fb
class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


# API endpoint for fetching user info
@api_view(['GET'])
@permission_classes([AllowAny])
def me(request):
    serializer = UserSerializer(request.user)
    return Response(serializer.data)


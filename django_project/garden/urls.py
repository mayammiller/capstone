from django.conf.urls import url
from . import views
from . import forms
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
app_name = 'garden'

urlpatterns = [
    url(r'^$', views.get_name, name='get_name'),
    url(r'^api/chart/data/$',views.ChartData.as_view()),
    url(r'^api/table/gardendata/', views.GardenTable.as_view()),
    url(r'^api/table/sensordata/', views.SensorTable.as_view()),
    url(r'^api/test-form/', views.ProfileDetail.as_view()),
    url(r'^api/table/gardendelete/$', views.DeleteGarden.as_view()),
    url(r'^api/table/plantdelete/$', views.DeletePlant.as_view()),
    url(r'^api/table/sensordelete/$', views.DeleteSensor.as_view()),
    #url(r'^$', views.get_plot, name='get_plot'),
    #path('', forms.AddGarden, name='new_garden'),
    #url(r'^$', views.GardenList.as_view(),name='single'),
    # path('', views.SensorListView.as_view(), name='person_changelist'),
    # path('', views.SensorCreateView.as_view(), name='sensor_add'),
    # path('<int:pk>/', views.SensorUpdateView.as_view(), name='sensor_change'),
    path('ajax/load-hardware/', views.load_hardware, name='ajax_load_hardware'),
    path('ajax/copy-hardware/', views.copy_hardware, name='ajax_copy_hardware'),
    path('loading_presets/', views.load_preset, name = 'load_preset_conditions'),
    path('loading_no_presets/', views.load_no_preset, name = 'no_conditions'),
    path('ajax/copied-hardware/', views.copied_hardware, name='ajax_copied_hardware'),
    # path('/garden/api/table/gardendata/', views.get_gardenTable, name="ajax_gardendata")
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

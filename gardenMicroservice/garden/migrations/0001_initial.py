# Generated by Django 2.2.2 on 2019-08-08 18:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ActiveUser',
            fields=[
                ('token', models.CharField(max_length=150, primary_key=True, serialize=False)),
                ('userid', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Garden',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('userid', models.IntegerField()),
                ('gardenName', models.CharField(max_length=150)),
            ],
            options={
                'unique_together': {('userid', 'gardenName')},
            },
        ),
        migrations.CreateModel(
            name='GardenPlant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('plantName', models.CharField(max_length=50)),
                ('plantQuantity', models.IntegerField(default=0)),
                ('plantLocation', models.CharField(blank=True, max_length=50)),
                ('garden', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Garden')),
            ],
        ),
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sensorID', models.CharField(blank=True, max_length=50)),
                ('sensorName', models.CharField(max_length=50)),
                ('sensorLocation', models.CharField(blank=True, max_length=50)),
                ('ispH', models.BooleanField(default='false')),
                ('isHumidity', models.BooleanField(default='false')),
                ('isLight', models.BooleanField(default='false')),
                ('isWaterLevel', models.BooleanField(default='false')),
                ('isNutrientConc', models.BooleanField(default='false')),
                ('isTemperature', models.BooleanField(default='false')),
                ('garden', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Garden')),
            ],
            options={
                'unique_together': {('garden', 'sensorID')},
            },
        ),
        migrations.CreateModel(
            name='PlantMetrics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('humidityLow', models.FloatField(blank=True)),
                ('humidityHigh', models.FloatField(blank=True)),
                ('lightLow', models.FloatField(blank=True)),
                ('lightHigh', models.FloatField(blank=True)),
                ('tempLow', models.FloatField(blank=True)),
                ('tempHigh', models.FloatField(blank=True)),
                ('nutrientLow', models.FloatField(blank=True)),
                ('nutrientHigh', models.FloatField(blank=True)),
                ('waterLow', models.FloatField(blank=True)),
                ('waterHigh', models.FloatField(blank=True)),
                ('pHLow', models.FloatField(blank=True)),
                ('pHHigh', models.FloatField(blank=True)),
                ('plant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.GardenPlant')),
            ],
        ),
        migrations.CreateModel(
            name='Control',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('controlID', models.CharField(max_length=50)),
                ('controlName', models.CharField(max_length=50)),
                ('controlLocation', models.CharField(blank=True, max_length=50)),
                ('ispH', models.BooleanField(default='false')),
                ('isHumidity', models.BooleanField(default='false')),
                ('isLight', models.BooleanField(default='false')),
                ('isWaterLevel', models.BooleanField(default='false')),
                ('isNutrientConc', models.BooleanField(default='false')),
                ('isTemperature', models.BooleanField(default='false')),
                ('garden', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Garden')),
            ],
            options={
                'unique_together': {('garden', 'controlID')},
            },
        ),
        migrations.CreateModel(
            name='WaterLevelSensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.FloatField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Sensor')),
            ],
            options={
                'unique_together': {('sensor', 'timestamp')},
            },
        ),
        migrations.CreateModel(
            name='TempSensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.FloatField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Sensor')),
            ],
            options={
                'unique_together': {('sensor', 'timestamp')},
            },
        ),
        migrations.CreateModel(
            name='PHSensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.FloatField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Sensor')),
            ],
            options={
                'unique_together': {('sensor', 'timestamp')},
            },
        ),
        migrations.CreateModel(
            name='NutrientSensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.FloatField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Sensor')),
            ],
            options={
                'unique_together': {('sensor', 'timestamp')},
            },
        ),
        migrations.CreateModel(
            name='LightSensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.FloatField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Sensor')),
            ],
            options={
                'unique_together': {('sensor', 'timestamp')},
            },
        ),
        migrations.CreateModel(
            name='HumiditySensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.FloatField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('sensor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Sensor')),
            ],
            options={
                'unique_together': {('sensor', 'timestamp')},
            },
        ),
        migrations.CreateModel(
            name='ControlStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=50)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('control', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.Control')),
            ],
            options={
                'verbose_name_plural': 'ControlStatuses',
                'unique_together': {('control', 'timestamp')},
            },
        ),
    ]

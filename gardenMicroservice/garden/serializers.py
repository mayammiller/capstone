from rest_framework import serializers
from .models import *

class GardenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garden
        # fields = ('userid','gardenName')
        fields = '__all__'

class GardenPlantSerializer(serializers.ModelSerializer):
    # garden = GardenSerializer()
    # garden_id = serializers.PrimaryKeyRelatedField(
    #     queryset=Garden.objects.all(), source='garden', write_only=True)
    class Meta:
        model = GardenPlant
        fields = '__all__'
        # depth = 1

    def to_representation(self, instance):
            response = super().to_representation(instance)
            response['garden'] = GardenSerializer(instance.garden).data
            return response
    #
    # def create(self, validated_data):
    #     gardens_data = validated_data.pop('garden')
    #     garden = Garden.objects.filter(userid=gardens_data['userid'], gardenName = gardens_data['gardenName'])
    #     if not garden:
    #         garden = Garden.objects.create(**gardens_data)
    #     gardenPlant = GardenPlant.objects.create(garden = garden, **validated_data)
    #     return gardenPlant

class PlantMetricsSerializer(serializers.ModelSerializer):
    # plant = GardenPlantSerializer()
    class Meta:
        model = PlantMetrics
        fields = '__all__'

class SensorSerializer(serializers.ModelSerializer):
    # garden = GardenSerializer()
    class Meta:
        model = Sensor
        fields = '__all__'

    def to_representation(self, instance):
            response = super().to_representation(instance)
            response['garden'] = GardenSerializer(instance.garden).data
            return response

class ControlSerializer(serializers.ModelSerializer):
    # garden = GardenSerializer()
    class Meta:
        model = Control
        fields = '__all__'

    def to_representation(self, instance):
            response = super().to_representation(instance)
            response['garden'] = GardenSerializer(instance.garden).data
            return response

class ControlStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'

class LightSensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'

class PHSensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'

class TempSensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'

class HumiditySensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'

class WaterLevelSensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'

class NutrientSensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'

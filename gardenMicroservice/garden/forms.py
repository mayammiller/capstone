# from django import forms
# from .models import *
# from django.contrib.auth.models import User
# from django.http import HttpResponse
# from django.forms import widgets
#
# #ALL THE PREDEFINED UNIT CHOICES ARE HERE!
#
# Type = (
#     ('s','Sensor'),
#     ('c','Control System')
#     )
#
# Variable = (
#     ('isLight','Light'),
#     ('isWaterLevel','Water Level'),
#     ('isNutrientConc','Nutrient Concentration'),
#     ('isTemperature','Temperature'),
#     ('isHumidity','Humidity'),
#     ('ispH','pH')
# )
#
# conductivityUnitTypes = (('tds','PPM'),('ec','mS'),('cf','cF'))
# tempUnitTypes = (('degC', '\N{DEGREE SIGN}C'),('degF','\N{DEGREE SIGN}F'))
#
# #UserGardens = [(x.plug_ip, x.gardenName) for x in Person.objects.filter(username = User)]
# #UserGardens = Person.objects.filter(username = User).values_list('id','gardenName')
#
# #ALL CLASSES RELATED TO CHECKING AND CONVERTING UNITS ARE HERE!
#
# class CondUnitWidget(widgets.MultiWidget):
#     def __init__(self, default_unit=None, attrs=None):
#         self.default_unit = default_unit
#         _widgets = [
#             widgets.NumberInput(attrs=attrs),
#             widgets.Select(attrs=attrs, choices=conductivityUnitTypes),
#         ]
#         super(CondUnitWidget, self).__init__(_widgets, attrs)
#     #convert a single value to list for the form
#     def decompress(self, value):
#         if value:
#             if self.default_unit == 'tds':
#                 return [value/700 , self.default_unit]
#             if self.default_unit == 'cf':
#                 return [value/10 , self.default_unit]
#             #value is the correct format
#             return [value, self.default_unit]
#         return [0, self.default_unit]
#
# class ElectroCond(forms.MultiValueField):
#     def __init__(self, *args, **kwargs):
#         _widget = CondUnitWidget(default_unit=kwargs['initial'][0])
#         fields = [
#             forms.FloatField(widget=forms.TextInput(attrs={'placeholder': '0.6'})),
#             forms.ChoiceField(choices=conductivityUnitTypes)
#         ]
#         super(ElectroCond, self).__init__(fields=fields, widget = _widget, *args, **kwargs)
#
#     def compress(self, values):
#         if values[1] == 'tds':
#             return  values[0] / 700
#         if values[1] == 'cf':
#             return  values[0] / 10
#         return values[0] #You dont need to convert because its in the format you want
#
# class TempUnitWidget(widgets.MultiWidget):
#     def __init__(self, default_unit=None, attrs=None):
#         self.default_unit = default_unit
#         _widgets = [
#             widgets.NumberInput(attrs=attrs),
#             widgets.Select(attrs=attrs, choices=tempUnitTypes),
#         ]
#         super(TempUnitWidget, self).__init__(_widgets, attrs)
#     #convert a single value to list for the form
#     def decompress(self, value):
#         if value:
#             if self.default_unit == 'degF':
#                 return [(value-32.2) * 5/9 , self.default_unit]
#             #value is the correct format
#             return [value, self.default_unit]
#         return [0, self.default_unit]
#
# class TemperatureFields(forms.MultiValueField):
#     def __init__(self, *args, **kwargs):
#         _widget = TempUnitWidget(default_unit=kwargs['initial'][0])
#         fields = [
#             forms.FloatField(),
#             forms.ChoiceField(choices=tempUnitTypes)
#         ]
#         super(TemperatureFields, self).__init__(fields=fields, widget = _widget, *args, **kwargs)
#
#     def compress(self, values):
#         if values[1] == 'degF':
#             return  (values[0]-32.3)*5/9
#         return values[0] #You dont need to convert because its in the format you want
#
# #FORMS BEGIN HERE!
#
# class AddGarden(forms.Form):  #form1
#     gardenName = forms.CharField(label="Garden Name",widget=forms.TextInput(attrs={'placeholder': 'My First Garden'}))
#     plantName = forms.CharField(label="What are you planting?",widget=forms.TextInput(attrs={'placeholder': 'Spinach'}))
#     plantQuantity = forms.IntegerField(label="How many plants?",widget=forms.TextInput(attrs={'placeholder': '3'}))
#     plantLocation = forms.CharField(label="Which section of the Garden (optional for large gardens)?",widget=forms.TextInput(attrs={'placeholder': 'SE'}), required=False)
#
# class AddPlants(forms.Form):  #form4
#
#     def __init__(self,*args,**kwargs):
#         choices = kwargs.pop('choices', None)
#         super(AddPlants, self).__init__(*args,**kwargs)
#         self.fields['gardenName'] = forms.ChoiceField(label="Garden Name", choices=choices)
#         self.fields['plantName'] = forms.CharField(label="What are you planting?",widget=forms.TextInput(attrs={'placeholder': 'Spinach'}))
#         self.fields['plantQuantity'] = forms.IntegerField(label="How many plants?",widget=forms.TextInput(attrs={'placeholder': '3'}))
#         self.fields['plantLocation'] = forms.CharField(label="Which section of the Garden (optional for large gardens)?",widget=forms.TextInput(attrs={'placeholder': 'SE'}), required=False)
#
#
# class AddSensor(forms.Form):  #form2
#
#     def __init__(self,*args,**kwargs):
#         choices = kwargs.pop('choices', None)
#         super(AddSensor, self).__init__(*args,**kwargs)
#         self.fields['gardenName'] = forms.ChoiceField(label="Garden Name", choices=choices)
#         self.fields['name'] = forms.CharField(label="Device Name", widget=forms.TextInput(attrs={'placeholder': 'My Device'}))
#         self.fields['deviceID'] = forms.CharField(label="Device ID (optional)",widget=forms.TextInput(attrs={'placeholder': '12345A'}), required=False)
#         self.fields['deviceLocation'] = forms.CharField(label="Device Location (optional for large gardens)",widget=forms.TextInput(attrs={'placeholder': 'SE'}), required=False)
#         self.fields['deviceType'] = forms.ChoiceField(label="Device Type", choices=Type)
#         self.fields['deviceParam'] =  forms.MultipleChoiceField(label="Measurements Available",widget=forms.CheckboxSelectMultiple,choices=Variable)
#
# class ModifySensor(forms.Form):  #form2
#     #THIS IS A FUTURE PROBLEM< BUT NEED TO BE ABLE TO EDIT SENSOR DATA
#     def __init__(self,*args,**kwargs):
#         choices = kwargs.pop('choices', None)
#         super(AddSensor, self).__init__(*args,**kwargs)
#         self.fields['gardenName'] = forms.ChoiceField(label="Garden Name", choices=choices)
#         self.fields['name'] = forms.ChoiceField(label="Device Name", widget=forms.TextInput(attrs={'placeholder': 'My Device'}))
#         self.fields['deviceID'] = forms.ChoiceField(label="Device ID (optional)",widget=forms.TextInput(attrs={'placeholder': '12345A'}), required=False)
#         self.fields['deviceLocation'] = forms.CharField(label="Device Location (optional for large gardens)",widget=forms.TextInput(attrs={'placeholder': 'SE'}), required=False)
#         self.fields['deviceType'] = forms.ChoiceField(label="Device Type", choices=Type)
#         self.fields['deviceParam'] =  forms.MultipleChoiceField(label="Measurements Available",widget=forms.CheckboxSelectMultiple,choices=Variable)
#
# # class ViewGardens(forms.Form):
# #     def __init__(self,choicesGarden, choicesSensors, choicesControls,*args,**kwargs):
# #         super(AddSensor, self).__init__(*args,**kwargs)
# #         self.fields['gardenName'] = forms.ChoiceField(label="Garden Name", choices=choices)
# #         self.fields['name'] = forms.CharField(label="Device Name", widget=forms.TextInput(attrs={'placeholder': 'My Device'}))
# #         self.fields['deviceID'] = forms.CharField(label="Device ID (optional)",widget=forms.TextInput(attrs={'placeholder': '12345A'}), required=False)
# #         self.fields['deviceLocation'] = forms.CharField(label="Device Location (optional for large gardens)",widget=forms.TextInput(attrs={'placeholder': 'SE'}), required=False)
# #         self.fields['deviceType'] = forms.ChoiceField(label="Device Type", choices=Type)
# #         self.fields['deviceParam'] =  forms.MultipleChoiceField(label="Measurements Available",widget=forms.CheckboxSelectMultiple,choices=Variable)
# class AddConditionsForm(forms.Form):  #form5
#         def __init__(self,*args,**kwargs):
#             choices = kwargs.pop('choices', None)
#             #need to get choices1 from view for the plants available for a given garden
#             super(AddConditionsForm, self).__init__(*args,**kwargs)
#             #self.fields['gardenName'] = forms.ChoiceField(label="Plant Name", choices=choices)
#             self.fields['plantName'] = forms.ChoiceField(label="Plant Name", choices=choices)
#             self.fields['humidityLow'] = forms.FloatField(label="Humidity Low Level (%)", widget=forms.TextInput(attrs={'placeholder': '40'}))
#             self.fields['humidityHigh'] = forms.FloatField(label="Humidity High Level (%)", widget=forms.TextInput(attrs={'placeholder': '70'}))
#             self.fields['lightLow'] = forms.FloatField(label="Light Low Level", widget=forms.TextInput(attrs={'placeholder': '0.4'}))
#             self.fields['lightHigh'] = forms.FloatField(label="Light High Level", widget=forms.TextInput(attrs={'placeholder': '0.9'}))
#             # self.fields['tempLow'] = forms.FloatField(label="Temperature Low Level", widget=forms.TextInput(attrs={'placeholder': '18'}))
#             self.fields['tempLow'] = TemperatureFields(label="Temperature Low Level", initial=[None,'degC'])
#             self.fields['tempLow'].widget.attrs.update({'placeholder':'18'})
#             # self.fields['tempHigh'] = forms.FloatField(label="Temperature High Level", widget=forms.TextInput(attrs={'placeholder': '26'}))
#             self.fields['tempHigh'] = TemperatureFields(label="Temperature High Level" , initial=[None,'degC'])
#             self.fields['tempHigh'].widget.attrs.update({'placeholder':'26'})
#             # self.fields['nutrientLow'] = forms.FloatField(label="Nutrient Low Level", widget=forms.TextInput(attrs={'placeholder': '0.6'}))
#             self.fields['nutrientLow'] = ElectroCond(label="Nutrient Low Level", initial=[None, 'ec'])
#             self.fields['nutrientLow'].widget.attrs.update({'placeholder': '0.8'})
#             # self.fields['nutrientHigh'] = forms.FloatField(label="Nutrient High Level", widget=forms.TextInput(attrs={'placeholder': '0.9'}))
#             self.fields['nutrientHigh'] = ElectroCond(label="Nutrient High Level", initial=[None, 'ec'])
#             self.fields['nutrientHigh'].widget.attrs.update({'placeholder': '1.2'})
#             self.fields['waterLow'] = forms.FloatField(label="Water Low Level (%)", widget=forms.TextInput(attrs={'placeholder': '20'}))
#             self.fields['waterHigh'] = forms.FloatField(label="Water High Level (%)", widget=forms.TextInput(attrs={'placeholder': '90'}))
#             self.fields['pHlow'] = forms.FloatField(label="pH Low Level", widget=forms.TextInput(attrs={'placeholder': '5.5'}))
#             self.fields['pHHigh'] = forms.FloatField(label="pH High Level", widget=forms.TextInput(attrs={'placeholder': '6.5'}))
# # ideal water temp based on https://www.gardeningknowhow.com/special/containers/hydroponic-water-temperature.htm (18-26 deg C, 65-80 degF)
# # ideal humidity levels should change over time (https://www.advancednutrients.com/articles/grow-room-temperature-humidity/), as per:
#         # Week 1 growth: 60%-70%
#         # Week 2 growth: 60%-70%
#         # Week 1 flower: 55%-65%
#         # Week 2 flower: 50%-60%
#         # Week 3 flower: 50%-55%
#         # Week 4 flower: 50%-
#         # Week 5 flower: 50%
#         # Week 6 flower: 45%
#         # Week 7 flower: 45%
#         # Week 8 flower: 40%
#         # Week 9 flower: 40%
# #ideal pH levels of between 5.5 and 6.5 for hydroponics (https://www.maximumyield.com/perfecting-ph/2/1212)
# class ViewGardenForm(forms.Form):  #form3
#
#     def __init__(self,*args, **kwargs):
#         choices = kwargs.pop('choices', None)
#         super(ViewGardenForm, self).__init__(*args, **kwargs)
#         self.fields['gardenNameView'] = forms.ChoiceField(label="Garden Name", choices=choices)
#         self.fields['sensorListView'] = forms.ChoiceField(choices=[],initial='0',widget=forms.SelectMultiple(),required=True,label='Available Sensors/Controls')
#         # self.fields['sensorListView'].queryset = Sensor.objects.none()
#
# class SelectGardenForm(forms.Form):  #form6
#     def __init__(self,*args, **kwargs):
#         super(SelectGardenForm, self).__init__(*args, **kwargs)
#         self.fields['whatToSeeList'] = forms.ChoiceField(label = "Sensors and Controls", choices = [],initial='0',widget=forms.SelectMultiple(),required=True)

from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView
# from .models import *
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic.edit import FormView
from .forms import *
from django.contrib.auth.models import User
from braces.views import SelectRelatedMixin
from django.views import generic
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib import messages
import re
import datetime
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import status
from .serializers import *
import json
from django.conf import settings
from .objects import *
from django.views.decorators.csrf import csrf_exempt

#from django.core import serializers
from rest_framework.renderers import JSONRenderer
from django.shortcuts import get_object_or_404
from rest_framework.renderers import TemplateHTMLRenderer
import requests
# import json

def get_choices(request):
    choices = [(0,'-------')]
    data = {'userid': request.user.id}
    r = requests.get(settings.GARDEN_API_URL + '/garden/api/list/garden/', params = data)
    data = r.json()
    choices=choices + [(x['id'], str(x['gardenName'])) for x in data]
    return choices

def get_choices2(request):
    data = {'userid': request.user.id}
    r2 = requests.get(settings.GARDEN_API_URL + '/garden/api/list/gardenplant/', params = data)
    data2 = r2.json()
    choices2 = [(x['id'], str(str(x['garden']['gardenName']) + ":" + str(x['plantName']))) for x in data2]
    return choices2


@login_required
def get_name(request):
    # if this is a POST request we need to process the form data
    choices = get_choices(request)
    choices2 = get_choices2(request)
    # choices= [(x.id, str(x.gardenName)) for x in Person.objects.filter(username = request.user)]
    if request.method == 'POST':
        print("got the POST request")
        if 'cancelRequest' in request.POST:
            form1 = AddGarden()
            form2 = AddSensor(choices=choices)
            form3 = ViewGardenForm(choices=choices)
            form4 = AddPlants(choices=choices)
            form5 = AddConditionsForm(choices=choices2)
            form6 = SelectGardenForm()

        if 'addNewGarden' in request.POST:
            form1 = AddGarden(request.POST)
            print(request.POST)

            if form1.is_valid():
                userid = request.user.id
                gardenName=form1.cleaned_data['gardenName']
                plantName=form1.cleaned_data['plantName']
                plantQuantity=int(form1.cleaned_data['plantQuantity'])
                plantLocation=form1.cleaned_data['plantLocation']

                myGarden = Garden(gardenName=gardenName, userid=userid)
                serializer = GardenSerializer(myGarden)
                print(serializer)
                data = json.dumps(serializer.data)
                myGardenPlant = GardenPlant(garden = myGarden, plantName = plantName, plantQuantity = plantQuantity, plantLocation = plantLocation)
                serializerGardenPlant = GardenPlantSerializer(myGardenPlant)
                # data={'serializer': serializerGardenPlant, 'token': active_token}
                data=json.dumps(serializerGardenPlant.data)
                r2 = requests.post(settings.GARDEN_API_URL + '/garden/api/list/gardenplant/',  headers={'Content-Type': 'application/json'},data = data)
                if (r2.status_code == 400):
                    messages.error(request, 'Garden could not be added: invalid user input')
                if (r2.status_code == 201):
                    messages.error(request, 'Garden successfully added')
            else:
                messages.error(request, 'Garden could not be added')

            form2 = AddSensor(choices=choices)
            form3 = ViewGardenForm(choices=choices)
            form4 = AddPlants(choices=choices)
            form5 = AddConditionsForm(choices=choices2)
            form6 = SelectGardenForm()

        elif 'addNewSensor' in request.POST:
            form2 = AddSensor(request.POST, choices=choices)
            if form2.is_valid():
                print(request.POST)
                id = form2.cleaned_data['gardenName']
                isPH = False
                isHum = False
                isLig = False
                isWat = False
                isNut = False
                isTemp = False
                if 'ispH' in form2.cleaned_data['deviceParam']: isPH = True
                if 'isHumidity' in form2.cleaned_data['deviceParam']: isHum = True
                if 'isLight' in form2.cleaned_data['deviceParam']: isLig = True
                if 'isWaterLevel' in form2.cleaned_data['deviceParam']: isWat = True
                if 'isNutrientConc' in form2.cleaned_data['deviceParam']: isNut = True
                if 'isTemperature' in form2.cleaned_data['deviceParam']: isTemp = True
                controlID=form2.cleaned_data['deviceID']
                controlName=form2.cleaned_data['name']
                controlLocation=form2.cleaned_data['deviceLocation']
                myGarden = Garden(id=id, userid=request.user.id)

                if(form2.cleaned_data['deviceType']=='c'):
                    print("went into saving to controls")
                    myControl = Control(garden=myGarden, controlID=controlID, controlName=controlName, controlLocation=controlLocation, \
                        ispH=isPH, isHumidity=isHum, isLight=isLig, isWaterLevel=isWat, isNutrientConc=isNut, isTemperature=isTemp)
                    serializer = ControlSerializer(myControl)
                    data=json.dumps(serializer.data)
                    r = requests.post(settings.GARDEN_API_URL + '/garden/api/list/control/', headers={'Content-Type': 'application/json'}, data = data)
                    if (r.status_code == 400):
                        messages.error(request, 'Sensor could not be added: invalid user input')
                    if (r.status_code == 201):
                        messages.error(request, 'Sensor successfully added')
                if(form2.cleaned_data['deviceType']=='s'):
                    mySensor = Sensor(garden=myGarden, sensorID=controlID, sensorName=controlName, sensorLocation=controlLocation, \
                        ispH=isPH, isHumidity=isHum, isLight=isLig, isWaterLevel=isWat, isNutrientConc=isNut, isTemperature=isTemp)
                    serializer = SensorSerializer(mySensor)
                    data=json.dumps(serializer.data)
                    r = requests.post(settings.GARDEN_API_URL + '/garden/api/list/sensor/', headers={'Content-Type': 'application/json'}, data = data)
                    if (r.status_code == 400):
                        messages.error(request, 'Sensor could not be added: invalid user input')
                    if (r.status_code == 201):
                        messages.error(request, 'Sensor successfully added')
                form1 = AddGarden()
                form3 = ViewGardenForm(choices=choices)
                form4 = AddPlants(choices=choices)
                form5 = AddConditionsForm(choices=choices2)
                form6 = SelectGardenForm()
        elif 'addNewPlants' in request.POST:
            form4 = AddPlants(request.POST, choices=choices)
            print("trying to add new plant")
            if form4.is_valid():
                userid = request.user.id
                gardenID=form4.cleaned_data['gardenName']
                plantName=form4.cleaned_data['plantName']
                plantQuantity=int(form4.cleaned_data['plantQuantity'])
                plantLocation=form4.cleaned_data['plantLocation']

                myGarden = Garden(id=gardenID, userid=userid)
                myGardenPlant = GardenPlant(garden = myGarden, plantName = plantName, plantQuantity = plantQuantity, plantLocation = plantLocation)
                serializerGardenPlant = GardenPlantSerializer(myGardenPlant)
                # data={'serializer': serializerGardenPlant, 'token': active_token}
                data=json.dumps(serializerGardenPlant.data)
                r2 = requests.post(settings.GARDEN_API_URL + '/garden/api/list/gardenplant/',  headers={'Content-Type': 'application/json'},data = data)
                if (r2.status_code == 400):
                    messages.error(request, 'Plant could not be added: invalid user input')
                if (r2.status_code == 201):
                    messages.error(request, 'Plant successfully added')
            else:
                messages.error(request, 'Plant could not be added')
            form1 = AddGarden()
            form2 = AddSensor(choices=choices)
            form3 = ViewGardenForm(choices=choices)
            form5 = AddConditionsForm(choices=choices2)
            form6 = SelectGardenForm()


        elif 'addPlantConditions' in request.POST:
            form5 = AddConditionsForm(request.POST, choices=choices2)
            print('adding plant conditions')
            if form5.is_valid():
                userid = request.user.id
                plantid = form5.cleaned_data['plantName']

                myGardenPlant = GardenPlant(id = plantid)
                humidityLow=form5.cleaned_data['humidityLow']
                humidityHigh=form5.cleaned_data['humidityHigh']
                lightLow=form5.cleaned_data['lightLow']
                lightHigh=form5.cleaned_data['lightHigh']
                tempLow=form5.cleaned_data['tempLow']
                tempHigh=form5.cleaned_data['tempHigh']
                nutrientLow=form5.cleaned_data['nutrientLow']
                nutrientHigh=form5.cleaned_data['nutrientHigh']
                waterLow=form5.cleaned_data['waterLow']
                waterHigh=form5.cleaned_data['waterHigh']
                pHLow=form5.cleaned_data['pHlow']
                pHHigh=form5.cleaned_data['pHHigh']
                if (humidityLow == None): humidityLow = 40
                if (humidityHigh == None): humidityHigh = 70
                if (lightLow == None): lightLow = 0.4
                if (lightHigh == None): lightHigh = 0.9
                if (tempLow == None): tempLow = 18
                if (tempHigh == None): tempHigh = 26
                if (nutrientHigh == None): nutrientHigh = 0.8
                if (nutrientLow == None): nutrientLow = 1.2
                if (waterLow == None): waterLow = 20
                if (waterHigh == None): waterHigh = 90
                if (pHLow == None): pHLow = 5.5
                if (pHHigh == None): pHHigh = 6.5

                myMetrics=PlantMetrics(plant=myGardenPlant, \
                        humidityLow=humidityLow, \
                        humidityHigh=humidityHigh,\
                        lightLow=lightLow,\
                        lightHigh=lightHigh,\
                        tempLow=tempLow,\
                        tempHigh=tempHigh,\
                        nutrientLow=nutrientLow,\
                        nutrientHigh=nutrientHigh,\
                        waterLow=waterLow,\
                        waterHigh=waterHigh,\
                        pHLow=pHLow,\
                        pHHigh=pHHigh)
                serializer = PlantMetricsSerializer(myMetrics)
                # data={'serializer': serializerGardenPlant, 'token': active_token}
                data=json.dumps(serializer.data)
                r2 = requests.post(settings.GARDEN_API_URL + '/garden/api/list/plantmetrics/',  headers={'Content-Type': 'application/json'},data = data)
                if (r2.status_code == 400):
                    messages.error(request, 'Plant could not be added: invalid user input')
                if (r2.status_code == 201):
                    messages.error(request, 'Plant successfully added')
            else:
                messages.error(request, 'Plant could not be added')
            form1 = AddGarden()
            form2 = AddSensor(choices=choices)
            form3 = ViewGardenForm(choices=choices)
            form4 = AddPlants(choices=choices)
            form6 = SelectGardenForm()
        else:
            form1 = AddGarden()
            form2 = AddSensor(choices=choices)
            form3 = ViewGardenForm(choices=choices)
            form4 = AddPlants(choices=choices)
            form5 = AddConditionsForm(choices=choices2)
            form6 = SelectGardenForm()

    # if a GET (or any other method) we'll create a blank form
    else:
        form1 = AddGarden()
        form2 = AddSensor(choices=choices)
        form3 = ViewGardenForm(choices=choices)
        form4 = AddPlants(choices=choices)
        form5 = AddConditionsForm(choices=choices2)
        form6 = SelectGardenForm()

    context = {
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'form5': form5,
        'form6': form6,
    }

    return render(request, 'garden/gardenView.html', context)
# class AddingGarden(FormView):
#     template_name = 'garden/gardenView.html'
#     form_class = AddGarden
#     def form_valid(self, form):
#         messages.success(request, f'Garden Successfully Added!')
#         # This method is called when valid form data has been POSTed.
#         # It should return an HttpResponse.
#         #form.send_email()
#         return super().form_valid(form)

# class SensorControl(object):
#     def __init__(self, gardenName, controlID, controlName, controlLocation, ispH, isHumidity, isLight, isWaterLevel, isNutrientConc, isTemperature):
#         self.gardenName = gardenName
#         self.controlID = controlID
#         self.controlName = controlName
#         self.controlLocation = controlLocation
#         self.ispH = ispH
#         self.isHumidity = isHumidity
#         self.isLight = isLight
#         self.isWaterLevel = isWaterLevel
#         self.isNutrientConc = isNutrientConc
#         self.isTemperature = isTemperature



def load_hardware(request):
    print("entered Load_hardware")
    garden_id = int(request.GET.get('gardenName'))
    #gardenName = Person.objects.filter(id=garden_id)
    #print(gardenName)
    sensors = []
    controls = []
    data = {'gardenid': garden_id}
    r_sensors = requests.get(settings.GARDEN_API_URL + '/garden/api/list/sensor/', params = data)
    r_controls = requests.get(settings.GARDEN_API_URL + '/garden/api/list/control/', params = data)
    print(r_sensors.json())
    # serializer = SensorSerializer(data=r_sensors.json(), many=True)
    # serializer.is_valid(raise_exception=True)
    # if serializer.is_valid():
    #     sensors = serializer.validated_data
    # print(sensors)
    # print(r_controls.json())
    # sensors = Sensor.objects.filter(gardenName__id=garden_id).order_by('sensorName')
    # #print(sensors)
    # controls = Control.objects.filter(gardenName__id=garden_id).order_by('controlName')
    if r_sensors.status_code == 200:
        sensors = r_sensors.json()
    if r_controls.status_code == 200:
        controls = r_controls.json()
    # controls = r_controls.json()
    return render(request, 'garden/gardenPlot.html', {'sensors': sensors, 'controls':controls})

def copied_hardware(request):
    print('entered copied hardware')
    controls=[]
    sensors=[]
    elements = request.GET.get('elements')
    mySensors = re.findall(r"S(\d+)",elements)
    myControls = re.findall(r"C(\d+)", elements)
    print(mySensors)
    r_sensors = requests.get(settings.GARDEN_API_URL + '/garden/api/list/sensor/', params = {'sensorID':mySensors})
    r_controls = requests.get(settings.GARDEN_API_URL + '/garden/api/list/control/', params = {'controlID': myControls})
    if r_sensors.status_code == 200:
        sensors = r_sensors.json()
    if r_controls.status_code == 200:
        controls = r_controls.json()
    # sensors = Sensor.objects.filter(id__in=mySensors).order_by('sensorName')
    # controls = Control.objects.filter(id__in=myControls).order_by('controlName')
    # print(sensors)
    # print(controls)
    return render(request, 'garden/gardenPlot.html', {'sensors': sensors, 'controls':controls})



def copy_hardware(request):
    pass


def load_preset(request):
    print("went into load preset conditions")
    #THIS FUNCTION NEEDS TO LOOK AT THE PLANTS IN THE GARDEN AND FILL OUT CONDITION LIMITS



def load_no_preset(request):
    pass
    #THIS FUNCTION NEEDS TO SET ALL THE GROWTH CONDITION LIMITS FOR THE BEDS TO NULL,
    #OR ELSE TRACK THAT NO CONTROLS ARE DESIRED


class DeleteGarden(APIView):
    # authentication_classes = [SessionAuthentication, BasicAuthentication]
    # permission_classes = [IsAuthenticated]
    authentication_classes = []
    permission_classes = []

    def delete(self,request):
        print('went into delete garden')
        gardenid = request.data['gardenid']
        data = {'gardenid': gardenid}
        r = requests.delete(settings.GARDEN_API_URL + '/garden/api/list/garden/', data = data)
        if (r.status_code == 204):
            messages.error(request, 'Garden deleted.')
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

class DeletePlant(APIView):
    # authentication_classes = [SessionAuthentication, BasicAuthentication]
    # permission_classes = [IsAuthenticated]
    authentication_classes = []
    permission_classes = []

    def delete(self,request):
        print('went into delete garden')
        plantid = request.data['plantid']
        data = {'plantid': plantid}
        r = requests.delete(settings.GARDEN_API_URL + '/garden/api/list/gardenplant/', data = data)
        if (r.status_code == 204):
            messages.error(request, 'Plant deleted.')
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

class DeleteSensor(APIView):
    authentication_classes = []
    permission_classes = []

    def delete(self,request):
        print('went into delete sensor')
        sensorid = request.data['sensorid']
        if (sensorid.startswith('S')):
            type='sensor'
            data = {'sensorid': sensorid[1:]}
            r = requests.delete(settings.GARDEN_API_URL + '/garden/api/list/sensor/', data = data)
        elif (sensorid.startswith('C')):
            type='control'
            data = {'controlid': sensorid[1:]}
            r = requests.delete(settings.GARDEN_API_URL + '/garden/api/list/control/', data = data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if (r.status_code == 204):
            if (type == 'sensor'): messages.error(request, 'Sensor deleted.')
            else:  messages.error(request, 'Control deleted.')
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

class ProfileDetail(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'profile_detail.html'

    def get(self, request, pk):
        profile = get_object_or_404(Garden, pk=pk)
        serializer = GardenSerializer(profile)
        return Response({'serializer': serializer, 'profile': profile})

    def post(self, request, pk):
        profile = get_object_or_404(Garden, pk=pk)
        serializer = GardenSerializer(profile, data=request.data)
        if not serializer.is_valid():
            return Response({'serializer2': serializer, 'profile': profile})
        serializer2.save()
        return redirect('profile-list')


class GardenTable(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    # permission_classes = []

    def get(self, request):
        userid=request.user.id
        data = {'userid': userid}
        print(data)
        r = requests.get(settings.GARDEN_API_URL + '/garden/api/table/gardendata/', params = data)
        return Response(r.json())

class SensorTable(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    # permission_classes = []

    def get(self, request):
        print('went to fetch sensor table data')
        userid=request.user.id
        data = {'userid': userid}
        print(data)
        r_sensors = requests.get(settings.GARDEN_API_URL + '/garden/api/list/sensor/', params = data)
        r_controls = requests.get(settings.GARDEN_API_URL + '/garden/api/list/control/', params = data)
        print(r_sensors.status_code)
        print(r_controls.status_code)
        return render(request, 'garden/sensorTable.html', {'sensors': r_sensors.json(), 'controls':r_controls.json()})
        # return Response({r.json())




class ChartData(APIView):

    authentication_classes = []
    permission_classes = []
    # borderColors = ['green','blue','red','purple','olive','maroon','gold','darkorange','brown','cyan']

    def get(self, request, format=None):
        print('went into get for chartdata')
        elements = request.GET.get('elements')
        print(elements)
        if not elements:
            # labels = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange']
            myData = [{'x':1,'y':5},{'x':2,'y':4},{'x':3,'y':6},{'x':4,'y':8},{'x':5,'y':3},{'x':6,'y':2}]
            data={
                "label":'red',
                "data":myData,
            }
            datasets = [data]
            # print("datasets")
            # print(datasets)
            return Response(datasets)

        r = requests.get(settings.GARDEN_API_URL + '/garden/api/chart/data/', params = {'elements':elements})
        return Response(r.json())

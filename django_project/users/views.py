from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from users.models import City
from .forms import UserRegisterForm, ProfileRegisterForm, UserUpdateForm, ProfileUpdateForm


def register(request):
    if request.method == 'POST':
        u_form = UserRegisterForm(request.POST)
        p_form = ProfileRegisterForm(request.POST)

        if u_form.is_valid() and p_form.is_valid():
            user = u_form.save()
            user.profile.street_address = p_form.cleaned_data.get('street_address')
            user.profile.city = p_form.cleaned_data.get('city')
            user.profile.province = p_form.cleaned_data.get('province')
            user.profile.postal_code = p_form.cleaned_data.get('postal_code')
            user.profile.image = p_form.cleaned_data.get('image')
            user.profile.save()
            messages.success(request, f'Your account has been created! You can now login!')
            return redirect('login')
    else:
        u_form = UserRegisterForm()  # for first time fetches all fields from forms.py
        p_form = ProfileRegisterForm()

    context = {
        'u_form': u_form, # renders those fetched fields to html form
        'p_form': p_form
    }

    return render(request, 'users/register.html',  context)
    # if validation doesnt go through it redirects to register page with data from form.user


@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }
    return render(request, 'users/profile.html', context)


def load_cities(request):
    province_id = request.GET.get('province')
    cities = City.objects.filter(province_id=province_id).order_by('name')
    return render(request, 'users/city_dropdown_list_options.html', {'cities': cities})


def landing_page(request):
    return render(request, 'users/index.html', {'title': 'LandingPage'})


def home(request):
    return render(request, 'users/home.html', {'title': 'HomePage'})
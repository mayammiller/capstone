# Create your views here.
from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView
from .models import *
from .serializers import *
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic.edit import FormView
from .forms import *
from django.contrib.auth.models import User
from braces.views import SelectRelatedMixin
from django.views import generic
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib import messages
import re
import datetime
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import serializers
from rest_framework import status
from rest_framework import request

#from django.core import serializers
from rest_framework.renderers import JSONRenderer
from django.shortcuts import get_object_or_404
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.parsers import JSONParser
# import json


# def get_name(request):
#     # if this is a POST request we need to process the form data
#     choices = [(0,'-------')]
#     choices=choices + [(x.id, str(x.gardenName)) for x in Person.objects.filter(username = request.user)]
#     choices2 = [(x.id, str(str(x.gardenName.gardenName) + ":" + str(x.plantName))) for x in Garden.objects.filter(gardenName__username = request.user)]
#     # choices= [(x.id, str(x.gardenName)) for x in Person.objects.filter(username = request.user)]
#     print("entered place to get POST request")
#     print(request)
#     if request.method == 'POST':
#         print(request.POST)
#         if 'cancelRequest' in request.POST:
#             form1 = AddGarden()
#             form2 = AddSensor(choices=choices)
#             form3 = ViewGardenForm(choices=choices)
#             form4 = AddPlants(choices=choices)
#             form5 = AddConditionsForm(choices=choices2)
#             form6 = SelectGardenForm()
#
#         if 'addNewGarden' in request.POST:
#             form1 = AddGarden(request.POST)
#             print(request.POST)
#             if form1.is_valid():
#                 print("went to save form 1")
#                 val=Person(username=request.user, gardenName=form1.cleaned_data['gardenName'])
#                 val.save()
#                 val2=Garden(gardenName=val, plantName=form1.cleaned_data['plantName'], plantQuantity=int(form1.cleaned_data['plantQuantity']), plantLocation=form1.cleaned_data['plantLocation'])
#                 val2.save()
#                 messages.success(request, 'Garden successfully added!')
#             form2 = AddSensor(choices=choices)
#             form3 = ViewGardenForm(choices=choices)
#             form4 = AddPlants(choices=choices)
#             form5 = AddConditionsForm(choices=choices2)
#             form6 = SelectGardenForm()
#                 #stuff here to same to db
#
#         elif 'addNewSensor' in request.POST:
#             form2 = AddSensor(request.POST, choices=choices)
#             if form2.is_valid():
#                 print(request.POST)
#                 isPH = False
#                 isHum = False
#                 isLig = False
#                 isWat = False
#                 isNut = False
#                 isTemp = False
#                 if 'ispH' in form2.cleaned_data['deviceParam']: isPH = True
#                 if 'isHumidity' in form2.cleaned_data['deviceParam']: isHum = True
#                 if 'isLight' in form2.cleaned_data['deviceParam']: isLig = True
#                 if 'isWaterLevel' in form2.cleaned_data['deviceParam']: isWat = True
#                 if 'isNutrientConc' in form2.cleaned_data['deviceParam']: isNut = True
#                 if 'isTemperature' in form2.cleaned_data['deviceParam']: isTemp = True
#                 print(request.POST)
#                 if(form2.cleaned_data['deviceType']=='c'):
#                     print("went into savinf to controls")
#                     myGarden = Person.objects.filter(username = request.user, id = form2.cleaned_data['gardenName']).get()
#                     try:
#                         val=Control(gardenName=myGarden, controlID=form2.cleaned_data['deviceID'], controlName=form2.cleaned_data['name'], controlLocation=form2.cleaned_data['deviceLocation'], \
#                             ispH=isPH, isHumidity=isHum, isLight=isLig, isWaterLevel=isWat, isNutrientConc=isNut, isTemperature=isTemp)
#                         val.save()
#                     except:
#                         print("didnt save to db")
#                     messages.success(request, 'Control successfully added!')
#                 if(form2.cleaned_data['deviceType']=='s'):
#                     myGarden = Person.objects.filter(username = request.user, id = form2.cleaned_data['gardenName']).get()
#
#                     val=Sensor(gardenName=myGarden, sensorID=form2.cleaned_data['deviceID'], sensorName=form2.cleaned_data['name'], sensorLocation=form2.cleaned_data['deviceLocation'], \
#                             ispH=isPH, isHumidity=isHum, isLight=isLig, isWaterLevel=isWat, isNutrientConc=isNut, isTemperature=isTemp)
#                     val.save()
#                     messages.success(request, 'Sensor successfully added!')
#                 form1 = AddGarden()
#                 form3 = ViewGardenForm(choices=choices)
#                 form4 = AddPlants(choices=choices)
#                 form5 = AddConditionsForm(choices=choices2)
#                 form6 = SelectGardenForm()
#         # elif 'addToView' in request.POST:
#         #     print("went into add to view")
#         #     print(request.POST['addToView'])
#         #     form3 = ViewGardenForm(request.POST, choices)
#         #
#         #     print(request.Post)
#         #     if form3.is_valid():
#         #         print('form3 is valid')
#         #         selected = form3.cleaned_data[sensorListView]
#         #         print(selected)
#         #
#         #
#         #     form1 = AddGarden()
#         #     form2 = AddSensor(choices=choices)
#         #     form4 = AddPlants(choices=choices)
#         #     form5 = AddConditionsForm(choices=choices2)
#         #     form6 = SelectGardenForm()
#
#
#
#
#         elif 'addNewPlants' in request.POST:
#             form4 = AddPlants(request.POST, choices=choices)
#             print("trying to add new plant")
#             print(request.POST)
#             print(form4.is_bound)
#             if form4.is_valid():
#                 print("form4 was valid")
#                 myGarden = Person.objects.filter(username = request.user, id = form4.cleaned_data['gardenName']).get()
#                 val=Garden(gardenName=myGarden, plantName=form4.cleaned_data['plantName'], plantQuantity=form4.cleaned_data['plantQuantity'], plantLocation=form4.cleaned_data['plantLocation'])
#                 val.save()
#                 messages.success(request, 'Plant successfully added!')
#
#             form1 = AddGarden()
#             form2 = AddSensor(choices=choices)
#             form3 = ViewGardenForm(choices=choices)
#             form5 = AddConditionsForm(choices=choices2)
#             form6 = SelectGardenForm()
#
#
#         elif 'addPlantConditions' in request.POST:
#             form5 = AddConditionsForm(request.POST, choices=choices2)
#             print("went into form5 check")
#             if form5.is_valid():
#                 myGarden = Garden.objects.filter(id = form5.cleaned_data['plantName']).get()
#                 print("valid form 5 to be saved")
#                 val=Plant(plantName=myGarden, \
#                         humidityLow=form5.cleaned_data['humidityLow'], \
#                         humidityHigh=form5.cleaned_data['humidityHigh'],\
#                         lightLow=form5.cleaned_data['lightLow'],\
#                         lightHigh=form5.cleaned_data['lightHigh'],\
#                         tempLow=form5.cleaned_data['tempLow'],\
#                         tempHigh=form5.cleaned_data['tempHigh'],\
#                         nutrientLow=form5.cleaned_data['nutrientLow'],\
#                         nutrientHigh=form5.cleaned_data['nutrientHigh'],\
#                         waterLow=form5.cleaned_data['waterLow'],\
#                         waterHigh=form5.cleaned_data['waterHigh'],\
#                         pHLow=form5.cleaned_data['pHlow'],\
#                         pHHigh=form5.cleaned_data['pHHigh'])
#                 val.save()
#                 messages.success(request, 'Conditions successfully added!')
#
#             form1 = AddGarden()
#             form2 = AddSensor(choices=choices)
#             form3 = ViewGardenForm(choices=choices)
#             form4 = AddPlants(choices=choices)
#             form6 = SelectGardenForm()
#
#         # elif '' in request.POST:
#         #     form6 = SelectGardenForm(request.POST)
#         #     if form6.is_valid():
#         #         pass
#         #         #remove the elements in the list from the graph and the list
#         #     form1 = AddGarden()
#         #     form2 = AddSensor(choices=choices)
#         #     form3 = ViewGardenForm(choices=choices)
#         #     form4 = AddPlants(choices=choices)
#         #     form5 = AddConditionsForm(choices=choices2)
#         #
#         else:
#             form1 = AddGarden()
#             form2 = AddSensor(choices=choices)
#             form3 = ViewGardenForm(choices=choices)
#             form4 = AddPlants(choices=choices)
#             form5 = AddConditionsForm(choices=choices2)
#             form6 = SelectGardenForm()
#
#     # if a GET (or any other method) we'll create a blank form
#     else:
#         form1 = AddGarden()
#         form2 = AddSensor(choices=choices)
#         form3 = ViewGardenForm(choices=choices)
#         form4 = AddPlants(choices=choices)
#         form5 = AddConditionsForm(choices=choices2)
#         form6 = SelectGardenForm()
#
#     context = {
#         'form1': form1,
#         'form2': form2,
#         'form3': form3,
#         'form4': form4,
#         'form5': form5,
#         'form6': form6,
#     }
#
#     return render(request, 'garden/gardenView.html', context)
# # class AddingGarden(FormView):
# #     template_name = 'garden/gardenView.html'
# #     form_class = AddGarden
# #     def form_valid(self, form):
# #         messages.success(request, f'Garden Successfully Added!')
# #         # This method is called when valid form data has been POSTed.
# #         # It should return an HttpResponse.
# #         #form.send_email()
# #         return super().form_valid(form)

########## AUTHENTICATION RELATED FUNCTIONS AND CLASSES ARE HERE ##########################
def checkAuthentication(token):
    theID = ActiveUser.objects.filter(token = token)
    if not theID:
        return Response("User ID does not match token", status=status.HTTP_401_UNAUTHORIZED)
    return Response(data = theID, status=status.HTTP_200_OK)

class TokenLog(APIView):

    def post(self,request):
        try:
            userid = request.data['userid']
            token = request.data['token']
            vals = ActiveUser(token = token, userid = userid)
            vals.save()
            return Response(userid, status=status.HTTP_201_CREATED)
        except Exception as error:
            Response(error, status=status.HTTP_400_BAD_REQUEST)


# def load_hardware(request):
#     gardenid = int(request.GET.get('garden_id'))
#     sensors = Sensor.objects.filter(garden__id=gardenid).order_by('sensorName')
#     controls = Control.objects.filter(garden__id=gardenid).order_by('controlName')
#     return render(request, 'garden/gardenPlot.html', {'sensors': sensors, 'controls':controls})

# class APILoadHardware(APIView):
#     authentication_classes = []
#     permission_classes = []
#
#     def get(self,request):
#         garden_id = int(request.data['gardenName'])
#         sensors = Sensor.objects.filter(garden__id=gardenid).order_by('sensorName')
#         controls = Control.objects.filter(garden__id=gardenid).order_by('controlName')
#         return render(request, 'garden/gardenPlot.html', {'sensors': sensors, 'controls':controls})





#
# def copied_hardware(request):
#     elements = request.GET.get('elements')
#     mySensors = re.findall(r"S(\d+)",elements)
#     myControls = re.findall(r"C(\d+)", elements)
#     sensors = Sensor.objects.filter(id__in=mySensors).order_by('sensorName')
#     controls = Control.objects.filter(id__in=myControls).order_by('controlName')
#     return render(request, 'garden/gardenPlot.html', {'sensors': sensors, 'controls':controls})
#


# class ProfileDetail(APIView):
#     renderer_classes = [TemplateHTMLRenderer]
#     template_name = 'profile_detail.html'
#
#     def get(self, request, pk):
#         profile = get_object_or_404(Person, pk=pk)
#         serializer = PersonSerializer(profile)
#         return Response({'serializer': serializer, 'profile': profile})
#
#     def post(self, request, pk):
#         profile = get_object_or_404(Profile, pk=pk)
#         serializer = ProfileSerializer(profile, data=request.data)
#         if not serializer.is_valid():
#             return Response({'serializer': serializer, 'profile': profile})
#         serializer.save()
#         return redirect('profile-list')








class APIGarden(APIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = GardenSerializer


    def post(self, request):
        print("entered APIGarden post class")
        print(request.data)
        serializer = GardenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self,request):
        userid = int(request.GET.get('userid'))
        gardens = Garden.objects.filter(userid=userid)
        serializer = GardenSerializer(gardens, many=True)
        return Response(serializer.data)

    def delete(self, request):
        id = request.data['gardenid']
        object = get_object_or_404(Garden, pk=id)
        object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


    # def post(self,request):
    #     print("entered APIGarden post class")
    #     print(request.POST.get('gardenName'))
    #     print(request.POST)
    #     userid = request.POST.get('userid')
    #     gardenName = request.POST.get('gardenName')
    #     plantName = request.POST.get('plantName')
    #     plantQuantity = request.POST.get('plantQuantity')
    #     plantLocation = request.POST.get('plantLocation')
    #     print("went to save form 1")
    #     try:
    #         val=Garden(userid=userid, gardenName=gardenName)
    #         val.save()
    #         val2=GardenPlant(gardenName=val, plantName=plantName, plantQuantity=int(plantQuantity), plantLocation=plantLocation)
    #         val2.save()
    #         messages.success(request, 'Garden successfully added!')
    #         return Response(status = status.HTTP_201_CREATED)
    #     except Exception as e:
    #         return Response(status = status.HTTP_400_BAD_REQUEST)


class APIGardenPlant(APIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = GardenPlantSerializer

    def post(self, request):
        print("entered APIGardenPlant post class")
        data = request.data
        gardens_data = data.pop('garden')
        if (gardens_data['gardenName'] is not None):
            garden = Garden.objects.filter(userid=gardens_data['userid'], gardenName = gardens_data['gardenName'])
            data['garden'] = garden[0].id
        else:
            garden = Garden.objects.filter(id=gardens_data['id'])
            data['garden'] = garden[0].id
        if not garden:
            garden = Garden.objects.create(**gardens_data)
            data['garden'] = garden.id
        print(data['garden'])

        serializer = GardenPlantSerializer(data=data)
        serializer.is_valid()
        print(serializer.errors)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self,request):
        userid = int(request.GET.get('userid'))
        gardens = GardenPlant.objects.filter(garden__userid=userid)
        serializer = GardenPlantSerializer(gardens, many=True)
        return Response(serializer.data)

    def patch(self, request, pk):
        plantObject = self.get_object(pk)
        serializer = GardenPlantSerializer(metricsObject, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        id = request.data['plantid']
        object = get_object_or_404(GardenPlant, pk=id)
        object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class APIPlantMetrics(APIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = PlantMetricsSerializer

    def post(self, request):
        print("entered APIPlantMetrics post class")
        print(request.data)
        data = request.data
        gardens_data = data.pop('plant')
        gardenplant = GardenPlant.objects.filter(id=gardens_data['id'])
        if not gardenplant:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        data['plant'] = gardenplant[0].id
        serializer = PlantMetricsSerializer(data=request.data)
        serializer.is_valid()
        print(serializer.errors)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk):
        metricsObject = self.get_object(pk)
        serializer = PlantMetricsSerializer(metricsObject, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class APISensor(APIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = SensorSerializer

    def post(self, request):
        print("entered APISensor post class")
        data = request.data
        print(data)
        gardens_data = data.pop('garden')
        garden = Garden.objects.filter(id=gardens_data['id'])
        if not garden:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        data['garden'] = garden[0].id
        serializer = SensorSerializer(data=data)
        serializer.is_valid()
        print(serializer.errors)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        print("entered APISensor get class")
        print(request.GET)
        if 'gardenid' in request.GET:
            gardenid = int(request.GET.get('gardenid'))
            sensors = Sensor.objects.filter(garden__id=gardenid).order_by('sensorName')
        elif 'sensorID' in request.GET:
            mySensors = (request.GET.get('sensorID'))
            sensors = Sensor.objects.filter(id__in=mySensors).order_by('sensorName')
        elif 'userid'in request.GET:
            mySensors = (request.GET.get('userid'))
            sensors = Sensor.objects.filter(garden__userid=mySensors).order_by('sensorName')
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        # print(sensors)
        if not sensors:
            return Response(status = status.HTTP_204_NO_CONTENT)
        serializer = SensorSerializer(sensors, many=True)
        # print(serializer.data)
        return Response(serializer.data)

    def delete(self, request):
        sensorid = request.data['sensorid']
        object = get_object_or_404(Sensor, pk=sensorid)
        object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    # def get(self, request, pk, format=None):
    #     snippet = self.get_object(pk)
    #     serializer = SnippetSerializer(snippet)
    #     return Response(serializer.data)


    # def post(self,request):
    #     print("entered AddSensor class")
    #     userid = request.POST.get('userid')
    #     gardenName = request.POST.get('gardenName')
    #     sensorID = request.POST.get('sensorID')
    #     sensorName = request.POST.get('sensorName')
    #     sensorLocation = request.POST.get('sensorLocation')
    #     ispH = request.POST.get('ispH')
    #     isHumidity = request.POST.get('isHumidity')
    #     isLight = request.POST.get('isLight')
    #     isWaterLevel = request.POST.get('isWaterLevel')
    #     isNutrientConc = request.POST.get('isNutrientConc')
    #     isTemperature = request.POST.get('isTemperature')
    #     myGarden = Garden.objects.filter(userid = userid, gardenName = gardenName).get()
    #     try:
    #         val=Sensor(gardenName=myGarden, sensorID=sensorID, sensorName=sensorName, sensorLocation=sensorLocation, \
    #             ispH=isPH, isHumidity=isHum, isLight=isLig, isWaterLevel=isWat, isNutrientConc=isNut, isTemperature=isTemp)
    #         val.save()
    #         return Response(status = status.HTTP_201_CREATED)
    #     except:
    #         return Response(status = status.HTTP_400_BAD_REQUEST)
    #     # serializer = SensorSerializer(data=data)
    #     # if (serializer.is_valid()):
    #     #     print("deserialized properly")
    #     # else:
    #     #     return Response(status=status.HTTP_400_BAD_REQUEST)

class APIControl(APIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = ControlSerializer

    def post(self, request):
        print("entered APIControl post class")
        data = request.data
        print(data)
        gardens_data = data.pop('garden')
        garden = Garden.objects.filter(id=gardens_data['id'])
        if not garden:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        data['garden'] = garden[0].id
        serializer = ControlSerializer(data=data)
        serializer.is_valid()
        print(serializer.errors)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        print("entered APIControl get class")
        if 'gardenid' in request.GET:
            gardenid = int(request.GET.get('gardenid'))
            controls = Control.objects.filter(garden__id=gardenid).order_by('controlName')
        elif 'controlID' in request.GET:
            myControls = (request.GET.get('controlID'))
            controls = Control.objects.filter(id__in=myControls).order_by('controlName')
        elif 'userid'in request.GET:
            myControls = (request.GET.get('userid'))
            controls = Control.objects.filter(garden__userid=myControls).order_by('controlName')
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if not controls:
            return Response(status = status.HTTP_204_NO_CONTENT)
        serializer = ControlSerializer(controls, many=True)
        return Response(serializer.data)

    def delete(self, request):
        controlid = request.data['controlid']
        object = get_object_or_404(Control, pk=controlid)
        object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    # def post(self,request):
    #     print("entered APIControl class")
    #     data = request.POST
    #     print(request.POST)
    #     serializer = ControlSerializer(data=data)
    #     if (serializer.is_valid()):
    #         print("deserialized properly")
    #         controlID = serializer.valdated_data.controlID
    #         controlName = serializer.valdated_data.controlName
    #         controlLocation = serializer.valdated_data.controlLocation
    #         isPH = serializer.valdated_data.isPH
    #         isHumidity = serializer.valdated_data.isHumidity
    #         isLight = serializer.valdated_data.isLight
    #         isWaterLevel = serializer.valdated_data.isWaterLevel
    #         isNutrientConc = serializer.valdated_data.isNutrientConc
    #         isTemperature = serializer.valdated_data.isTemperature
    #         userid = serializer.valdated_data.user_id
    #         gardenName = serializer.valdated_data.gardenName
    #
    #         myGarden = Person.objects.filter(username = userid, gardenName = gardenName).get()
    #         try:
    #             val=Control(gardenName=myGarden, controlID=controlID, controlName=controlName, controlLocation=controlLocation, \
    #                 ispH=isPH, isHumidity=isHum, isLight=isLig, isWaterLevel=isWat, isNutrientConc=isNut, isTemperature=isTemp)
    #             val.save()
    #             return Response(status = status.HTTP_201_CREATED)
    #         except:
    #             return Response(status = status.HTTP_400_BAD_REQUEST)
    #     else:
    #         return Response(status=status.HTTP_400_BAD_REQUEST)




class GardenTable(APIView):
    authentication_classes = []
    permission_classes = []
    # permission_classes = []

    def get(self, request):
        # userid = request.GET.get('userid')
        # print('went into garden table get')
        userid = int(request.GET.get('userid'))
        # print(userid)
        myPlants = GardenPlant.objects.filter(garden__userid=userid).order_by('garden__gardenName')#.values("gardenName__gardenName","plantName")
        serializer = GardenPlantSerializer(myPlants, many=True)
        return Response(JSONRenderer().render(serializer.data))
        # return Response(serializer.data)



class ChartData(APIView):

    authentication_classes = []
    permission_classes = []
    borderColors = ['green','blue','red','purple','olive','maroon','gold','darkorange','brown','cyan']

    def get(self, request, format=None):
        print('went into get for chart data')
        # elements = request.GET.get('elements')
        # if not elements:
        #     # labels = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange']
        #     myData = [{'x':1,'y':5},{'x':2,'y':4},{'x':3,'y':6},{'x':4,'y':8},{'x':5,'y':3},{'x':6,'y':2}]
        #     data={
        #         "label":'red',
        #         "data":myData,
        #     }
        #     datasets = [data]
        #     return Response(datasets)
        # print(elements)
        elements = request.GET.get('elements')
        mySensors = re.findall(r"S(\d+)",elements)
        myControls = re.findall(r"C(\d+)", elements)
        sensors = Sensor.objects.filter(id__in=mySensors).order_by('sensorName')
        controls = Control.objects.filter(id__in=myControls).order_by('controlName')
        datasets = []
        numOfLines = 0
        for sensor in sensors:
            if (numOfLines > len(ChartData.borderColors)):
                break
            if (sensor.ispH):
                theData = PHSensor.objects.filter(sensor=sensor).order_by('timestamp')
                if(theData):
                    datasets.append(self.getData(sensor, theData,numOfLines))
                    numOfLines += 1
            if (sensor.isHumidity):
                theData = HumiditySensor.objects.filter(sensor=sensor).order_by('timestamp')
                if(theData):
                    datasets.append(self.getData(sensor, theData,numOfLines))
                    numOfLines += 1
            if (sensor.isLight):
                theData = LightSensor.objects.filter(sensor=sensor).order_by('timestamp')
                if(theData):
                    datasets.append(self.getData(sensor, theData,numOfLines))
                    numOfLines += 1
            if(sensor.isWaterLevel):
                theData = WaterLevelSensor.objects.filter(sensor=sensor).order_by('timestamp')
                if(theData):
                    datasets.append(self.getData(sensor, theData,numOfLines))
                    numOfLines += 1
            if(sensor.isNutrientConc):
                theData = NutrientSensor.objects.filter(sensor=sensor).order_by('timestamp')
                if(theData):
                    datasets.append(self.getData(sensor, theData,numOfLines))
                    numOfLines += 1
            if(sensor.isTemperature):
                theData = TempSensor.objects.filter(sensor=sensor).order_by('timestamp')
                if(theData):
                    datasets.append(self.getData(sensor, theData,numOfLines))
                    numOfLines += 1

                # datasets.append(thisData)
        for count,control in enumerate(controls):
            numOfLines += 1
            if (numOfLines > len(ChartData.borderColors)):
                break
            theData = ControlStatus.objects.filter(control=control).order_by('timestamp')
            if theData:
                label=control.controlName
                data = []
                for datas in theData:
                    x = datas.timestamp
                    if "ON" in datas.status:
                        y = 1
                    elif "OFF" in data.status:
                        y = 0
                    else:
                        y = -1
                    thedict = {'x':x, 'y':y}
                    data.append(thedict)
                thisData = {'label': label,
                            'borderColor': ChartData.borderColors[numOfLines],
                            'data' : data}
                datasets.append(thisData)

        return Response(datasets)

    def getData(self,sensor, theData,numOfLines):
        if(theData):
            label=sensor.sensorName
            data = []
            for datas in theData:
                x = datas.timestamp
                y = datas.value
                # x = x.strftime("%Y-%m-%d %H:%M:%S")
                # 'YYYY-MM-DD HH:mm:ss'
                # might still need to do something here for timezone changes
                # print(x.strftime("%a, %d %b %Y %H:%M:%S %Z"))
                thedict = {'x':x, 'y':y}
                data.append(thedict)
            thisData = {'label': label,
                        'borderColor': ChartData.borderColors[numOfLines],
                        'data' : data}
            return thisData
        return {}
# class SensorListView(ListView):
#     model = Sensor
#     form_class = ViewGardenForm
    #context_object_name = 'gardenName'

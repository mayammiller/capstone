from rest_framework import serializers
from .models import *

# class GardenSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Garden
#         fields = '__all__'
#         depth = 1
#
# class PersonSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Person
#         fields = '__all__'#('username','gardenName')#'__all__'
#         depth = 2


class GardenSerializer(serializers.Serializer):
    userid = serializers.IntegerField()
    gardenName = serializers.CharField(max_length=150)
    id = serializers.IntegerField()

    def create(self, validated_data):
        return Task(id=None, **validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        return instance

class GardenPlantSerializer(serializers.Serializer):
    garden = GardenSerializer()
    plantName = serializers.CharField(max_length=50)
    plantQuantity = serializers.IntegerField(default=0)
    plantLocation = serializers.CharField(max_length=50,required=False)
    id = serializers.IntegerField()

    # def create(self, validated_data):
    #     # Create the book instance
    #     book = Book.objects.create(title=validated_data['title'])
    #
    #     # Create or update each page instance
    #     for item in validated_data['pages']:
    #         garden = Page(id=item['page_id'], text=item['text'], book=book)
    #         page.save()

        # return book
    # class Meta:
    #         depth = 1

class PlantMetricsSerializer(serializers.Serializer):
    plant = GardenPlantSerializer()
    humidityLow = serializers.FloatField()
    humidityHigh = serializers.FloatField()
    lightLow = serializers.FloatField()
    lightHigh = serializers.FloatField()
    tempLow = serializers.FloatField()
    tempHigh = serializers.FloatField()
    nutrientLow = serializers.FloatField()
    nutrientHigh = serializers.FloatField()
    waterLow = serializers.FloatField()
    waterHigh = serializers.FloatField()
    pHLow = serializers.FloatField()
    pHHigh = serializers.FloatField()

class SensorSerializer(serializers.Serializer):
    garden = GardenSerializer()
    sensorID = serializers.CharField(max_length=50, )
    sensorName = serializers.CharField(max_length=50)
    sensorLocation = serializers.CharField(max_length=50, required=False)
    ispH = serializers.BooleanField(default = 'false')
    isHumidity = serializers.BooleanField(default = 'false')
    isLight = serializers.BooleanField(default = 'false')
    isWaterLevel = serializers.BooleanField(default = 'false')
    isNutrientConc = serializers.BooleanField(default = 'false')
    isTemperature = serializers.BooleanField(default = 'false')

class ControlSerializer(serializers.Serializer):
    garden = GardenSerializer()
    controlID = serializers.CharField(max_length=50)
    controlName = serializers.CharField(max_length=50)
    controlLocation = serializers.CharField(max_length=50, required=False)
    ispH = serializers.BooleanField(default = 'false')
    isHumidity = serializers.BooleanField(default = 'false')
    isLight = serializers.BooleanField(default = 'false')
    isWaterLevel = serializers.BooleanField(default = 'false')
    isNutrientConc = serializers.BooleanField(default = 'false')
    isTemperature = serializers.BooleanField(default = 'false')

class ControlStatusSerializer(serializers.Serializer):
    control = ControlSerializer()
    status = serializers.CharField(max_length=50)
    timestamp = serializers.DateTimeField()

    # class Meta:
    #     verbose_name_plural = "ControlStatuses"

class LightSensorSerializer(serializers.Serializer):
    sensor = SensorSerializer()
    value = serializers.FloatField()
    timestamp = serializers.DateTimeField()

class PHSensorSerializer(serializers.Serializer):
    sensor = SensorSerializer()
    value = serializers.FloatField()
    timestamp = serializers.DateTimeField()

class TempSensorSerializer(serializers.Serializer):
    sensor = SensorSerializer()
    value = serializers.FloatField()
    timestamp = serializers.DateTimeField()

class HumiditySensorSerializer(serializers.Serializer):
    sensor = SensorSerializer()
    value = serializers.FloatField()
    timestamp = serializers.DateTimeField()

class WaterLevelSensorSerializer(serializers.Serializer):
    sensor = SensorSerializer()
    value = serializers.FloatField()
    timestamp = serializers.DateTimeField()

class NutrientSensorSerializer(serializers.Serializer):
    sensor = SensorSerializer()
    value = serializers.FloatField()
    timestamp = serializers.DateTimeField()

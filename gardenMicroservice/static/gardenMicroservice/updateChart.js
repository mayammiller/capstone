$(document).ready(function(){ //initial update of table with junk data
  var endpoint = '/garden/api/chart/data/'
  var chartData = []
  var chartLabels=[]
    $.ajax({
      method:"GET",
      url: endpoint,
      success: function(data){
        updateChart(data,)
      },
      error: function(error_data){
        console.log("error")
        console.log(error_data)
      }
    })
})

var myTable = [] //purpose of this script is to load the data from table of sensors and controls into table of things to view

function updateForm6(){ //updates form6 based on myTable element, and implicitly updates chart
  var endpoint = '/garden/api/chart/data/'
  var url = $("#SelectedHardwareForm").attr("data-hardware-url");
  console.log(JSON.stringify(myTable))
  $.ajax({ //updates form6 to include all highlighted elements
    url: url,
    data: {
      'elements': JSON.stringify(myTable)       // add the country id to the GET parameters
    },
    success: function (data) {   // `data` is the return of the `load_cities` view function
      // $("#myTable").html(data);  // replace the contents of the city input with the data that came from the server
      console.log(data)
      $("#id_whatToSeeList").html(data);

    }});

  $.ajax({ //gets the data from the sensors to be plotted (configures datetime to date)
      url: endpoint,
      data: {
        'elements': JSON.stringify(myTable)       // add the country id to the GET parameters
      },
      success: function (data) {   // `data` is the return of the `load_cities` view function
        for (var i = 0; i < data.length; i++) {
          for (var j = 0; j < data[i].data.length; j++) {
            data[i].data[j].x = moment(data[i].data[j].x,"YYYY-MM-DD HH:mm:ss");
          }
        }
        console.log(data)
        updateChart(data)
      }});
  }

function getElementsToView(){ //adds highlighted elements from list of controls to form6 (implicitly to chart)
  var values = $('#id_sensorListView').val();
  // console.log(values)
  myTable.push(values)
  updateForm6()
}

function deleteFromForm(){ //deletes the form (and implicitly from chart) when delete button pressed
  console.log("went into deletefromForm")
  var values = $('#id_whatToSeeList').val();
  console.log(values)
  myTable.pop(values)
  updateForm6()
  //update to other table HERE!
}


function updateChart(chartData){ //given the properly formatted chart data, will replot the chart data
  var ctx = document.getElementById('myChart').getContext('2d');
  // console.log(chartData)
  var myChart = new Chart(ctx, {
      type: 'scatter',
      data: {
        datasets: chartData
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }],
              xAxes: [{
                  type: 'time',
                      distribution: 'linear',
                  time: {
                    displayFormats: {
                      quarter: 'MMM YYYY'
                  }}
              }]
          }
      }
  });
}
